function uniformAngle() {
  return 2 * Math.PI * Math.random();
}

function standardNormal() {
  standardNormal.generate = !standardNormal.generate;
  if (standardNormal.generate) {
    // There's a cached result.
    return standardNormal.y;
  }
  do {
   var u = Math.random();
   var v = uniformAngle();
  } while (u < Number.MIN_VALUE);
  let factor = Math.sqrt(-2 * Math.log(u));
  let x = factor * Math.cos(v);
  standardNormal.y = factor * Math.sin(v);
  return x;
}