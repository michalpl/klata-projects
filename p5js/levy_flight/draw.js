var x = [];
var y = [];

var intX = [];
var intY = [];

var alphas;
var first = true;

var fromc;
var to;
const maxColor = 1000;
var iteration;
var rigth;

function setup() {  
	createCanvas(windowWidth, windowHeight);

	x[0] = 0;
	y[0] = 0;

	intX[0] = 0
	intY[0] = 0;

	alphas = 1.8;
	fromc = color(255, 0, 0);
	to = color(0, 0, 255);
	right = true;
	iteration = 0;
	
	background(255);
	frameRate(120);
}


function draw() {
	if (iteration === 0) right =  true;
	else if (iteration === 1000) right = false;
	
	var aColor = lerpColor(fromc, to, iteration / maxColor);
	
	stroke(aColor);
	
	do {
		var th = random(0.0, 1.0) * 2 * PI;
		var tmp = random(0.0, 1.0);
		var f = pow(tmp, -1 / alphas);		
	} while ((((x[0] + f * cos(th)) > 250) || (y[0] + f * sin(th) > 250)) 
		|| (((x[0] + f * cos(th)) < -250) || (y[0] + f * sin(th)) < -250));
	
	x.push(x[0] + f * cos(th));
	y.push(y[0] + f * sin(th));
	intX.push(round(((abs(x[0] + f * cos(th))) / 250 ) * windowWidth));
	intY.push(round(((abs(y[0] + f * sin(th))) / 250 ) * windowHeight));
	
	line(intX[0], intY[0], intX[1], intY[1]);
	
	//if (!first) line(intX[0], intY[0], intX[1], intY[1]);
	//else {
	//	line(windowWidth / 2, windowHeight / 2, intX[1], intY[1]);
	//	first = false;
	//}
	console.log("x:" + x[0] + " y:" + y[0]);

	x.splice(0, 1);
	y.splice(0, 1);

	intX.splice(0, 1);
	intY.splice(0, 1);
	
	if (right) iteration++;
	else iteration--;
}

//https://math.stackexchange.com/questions/52869/numerical-approximation-of-levy-flight