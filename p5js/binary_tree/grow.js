grow = function(table, startIndex) {
	if (startIndex < table.length) {
		if (table[startIndex].show()) {
			grow(table, startIndex + startIndex + 1);
			return grow(table, startIndex + startIndex + 2);
		}
	} else return true;
}