var tree = [];
var leaves = [];

var counter = 0;
var finished = false;

function setup() {
	createCanvas(windowWidth,windowHeight);

	var a = createVector(width / 2, height);
	var b = createVector(width / 2, height - (height / 3));
	var thickness = 8;
	var bonusAngel1 = random(-PI/2, PI/2);
	var bonusAngel2 = random(-PI/2, PI/2);

	var G = 80;

	var root = new Branch(a, b, thickness, G, bonusAngel1, bonusAngel2);
	tree[0] = root;

	for (var j = 1; j < 9; j++) {	
		for (var i = tree.length - 1; i >= 0; i--) {
			if ((!tree[i].finished)) {
				tree.push(tree[i].branchLeft());
				tree.push(tree[i].branchRight());
			}
			tree[i].finished = true;
		}
	}

	for (var i = 0; i < tree.length; i++) {
		if (!tree[i].finished) {
			var leaf = new Leaves(tree[i].end.copy(), random(5,56), 1, random(4,10), random(255), random(255), random(255), random(80,250));
			leaves.push(leaf);
		}
	}
}

function mousePressed() {
	if (leaves[leaves.length - 1].finished) { 
		for (var i = 0; i < leaves.length; i++) {
			var d = dist(mouseX, mouseY, leaves[i].cords.x, leaves[i].cords.y);
			if (d < (leaves[i].thickness / 2)) leaves[i].fall = true;
		}
	}
}

function draw() {
	background(51);
	if (grow(tree, 0)) {
		for (var i = 0; i < leaves.length; i++) {
			if (leaves[i].fall && (leaves[i].cords.y < height + leaves[i].thickness)) {
				leaves[i].cords.y += leaves[i].fallSpeedTest;
			}
			
			leaves[i].show();
		}
	}
}