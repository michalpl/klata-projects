function Leaves(cords, thickness, growspeed, fallSpeed, R, G, B, fillOpacity) {
	this.cords = cords;
	
	this.thickness = thickness;
	this.growspeed = growspeed;
	this.fallSpeed = fallSpeed;
	this.fallSpeedTest = fillOpacity * thickness * 0.001;

	this.R = R;
	this.G = G;
	this.B = B;
	this.fillOpacity = fillOpacity;

	this.fall = false;
	this.finished = false;

	this.startThickness = 0;

	this.show = function() {
		fill(this.R, this.G, this.B, this.fillOpacity);
		tint(255, 127);
		noStroke()
		ellipse(this.cords.x, this.cords.y, this.startThickness, this.startThickness);
		if (this.startThickness < this.thickness) this.startThickness += this.growspeed;
		else {
			this.finished = true;
			return true;
		}
	}
}