function Branch(begin, end, thickness, G, bonusAngel1, bonusAngel2) {
	this.begin = begin;
	this.end = end;
	this.thickness = thickness;
	this.bonusAngel1 = bonusAngel1;
	this.bonusAngel2 = bonusAngel2;

	this.G = G;

	this.finished = false;
	this.angel = 6 * PI / 5; 

	this.endY = this.begin.y;
	this.endX = this.begin.x;
	
	this.lineXFinished = false;
	this.lineYFinished = false;

	this.speed = 3;

	this.show = function() {
		stroke(255, this.G, 0);
		//stroke(this.RGB.R, this.RGB.G, this.RGB.B);
		strokeWeight(this.thickness);

		line(this.begin.x, this.begin.y, this.endX, this.endY);

		// Y grow
		if (this.begin.y < this.end.y)
		{
			if (this.endY < (this.end.y)) this.endY += this.speed;
			else this.lineYFinished = true;
		} else {
			if (this.endY > (this.end.y)) this.endY -= this.speed;
			else this.lineYFinished = true;
		}

		// X grow
		if (this.begin.x < this.end.x)
		{
			if (this.endX < (this.end.x)) this.endX += this.speed;
			else this.lineXFinished = true;
		} else {
			if (this.endX > (this.end.x)) this.endX -= this.speed;
			else this.lineXFinished = true;
		}

		// Return true when finished drowing
		return this.lineXFinished && this.lineYFinished;
	}

	this.branchLeft = function() {
		var dir = p5.Vector.sub(this.end, this.begin);		
		
		dir.rotate(this.bonusAngel1);
		//dir.rotate(-PI/10);
		
		dir.rotate(this.angel);
		dir.mult(0.67);

		var newEnd = p5.Vector.sub(this.end, dir);

		if (this.thickness > 1) {
			var left = new Branch(this.end, newEnd, this.thickness - 1, this.G + 10, this.bonusAngel1, this.bonusAngel2)
		} else var left = new Branch(this.end, newEnd, 1, this.G + 10, this.bonusAngel1, this.bonusAngel2);

		return left;
	}

	this.branchRight = function() {
		var dir = p5.Vector.sub(this.end, this.begin);		

		dir.rotate(this.bonusAngel2);
		//dir.rotate(-PI/5);

		dir.rotate(-this.angel);
		dir.mult(0.67);

		var newEnd = p5.Vector.sub(this.end, dir);

		if (this.thickness > 1) {
			var right = new Branch(this.end, newEnd, this.thickness - 1, this.G + 10, this.bonusAngel1, this.bonusAngel2)
		} else var right = new Branch(this.end, newEnd, 1, this.G + 10, this.bonusAngel1, this.bonusAngel2);
		
		return right;
	}


}