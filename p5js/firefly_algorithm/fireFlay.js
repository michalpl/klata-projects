function FireFlay(x, y) {
	this.x = x;
	this.y = y;
	this.glow;

	this.show = function(rx, ry, best) {
		
		if (!best) fill(51);
		else fill(255, 100, 0);

		if ((rx + this.x < 2) && (rx + this.x > -2)) this.x += rx;
		else this.x -= rx;
		if ((ry + this.y < 2) && (ry + this.y > -2)) this.y += ry;
		else this.y -= ry;
		
		var intX = round(((this.x + 2) / 4) * windowWidth);
		var intY = round(((this.y + 2) / 4) * windowHeight);

		//this.glow = rosenbrock(this.x, this.y);

		ellipse(intX, intY, 10, 10);
	}

	this.calcGlow = function(rx, ry) {
		this.glow = rosenbrock(this.x, this.y);
	}
}