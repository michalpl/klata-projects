function distance(x1, x2, y1, y2) {
	return sqrt(sq(x2 - x1) + sq(y2 - y1));
}