var fireFlays = [];
var bestglow;
var bestx, besty, best;
var iterations;
var step;
var input, button;
var population;
var startInterval, endInterval;
var bg;

function setup() {  
	bg = loadImage("function.jpg");

	createCanvas(windowWidth, windowHeight);

	population = 20;
	startInterval = -2.0;
	endInterval = 2.0;

	input = createInput(20, "number", "min", 5, "max", 100);
	input.position(10, 130);
	input.value(population);

	button = createButton('OK', "submit");
	button.position(input.x + input.width, 130);
	button.mousePressed(reset);

	frameSlider = createSlider(1, 60, 5);
 	frameSlider.position(button.x + button.width + 10, 130);

 	background(bg);
	textSize(20);
	fill(51);
	frameRate(5);

	reset();

// for make function
	// var r = 255;
	// var g = 255;
	// var b = 255;
	// var rgbValue;
	// noLoop();
}

function reset() {
	population = input.value();
	fireFlays = [];

	for (var i = 0; i < population; i++) {
		fireFlays.push(new FireFlay(random(startInterval, endInterval), random(startInterval, endInterval)));
		fireFlays[i].calcGlow(0, 0);
		
		if (i == 0) bestglow = fireFlays[i].glow;
		else if (fireFlays[i].glow > bestglow) {
			bestglow = fireFlays[i].glow;
			best = i;
			bestx = fireFlays[i].x;
			besty = fireFlays[i].y;
		}
	}

	iterations = 0;
	step = 0.1;
}

function draw() {
	// for make function

	// for (var i = 0; i < windowWidth; i++) {
	// 	for (var j = 0; j < windowHeight; j++) {
	// 		r = 255;
	// 		g = 255;
	// 		b = 255;
	// 		rgbValue = rosenbrock(((i / windowWidth) * 4 - 2), ((j / windowHeight) * 4 - 2));

	// 		var tmp = abs(rgbValue / (- 100));

	// 		var iter = round(tmp * 750);
	// 		if (rgbValue >= -100)
	// 		{
	// 			console.log(i + "," + j + "," + iter);
	// 			if (r - iter < 0) {
	// 				tmp -= r;
	// 				r = 0;
	// 				if (g - iter < 0) {
	// 					tmp -= g;
	// 					g = 0;
	// 					b -= iter;
	// 				} else g -= iter;
	// 			} else r -= iter;
	// 		}
	// 		else {
	// 			r = 0;
	// 			g = 0;
	// 			if (b - (iter - 500) < 256) b -= (iter - 500);
	// 			else b = 0;
	// 		}

	// 		stroke(r, g, b);
	// 		point(i, j)
	// 		//rgbValue
	// 	}
	// }
	// saveFrames("out", "png", 1, 1);

	if (iterations == 2000) reset();
	background(bg);
	//image(bg, 0, 0);
	frameRate(frameSlider.value());

	stroke(0);
	//oś y
	line(10, windowHeight / 2, windowWidth - 10, windowHeight / 2);
	
	//oś x
	line(windowWidth / 2, 10, windowWidth / 2, windowHeight - 10);
	
	//skala na osi x
	line(round((3/(abs(endInterval - startInterval))) * windowWidth), (windowHeight / 2) - 10, round((3/(abs(endInterval - startInterval))) * windowWidth), (windowHeight / 2) + 10);
	line(round((1/(abs(endInterval - startInterval))) * windowWidth), (windowHeight / 2) - 10, round((1/(abs(endInterval - startInterval))) * windowWidth), (windowHeight / 2) + 10);
	
	//skala na osi y
	line((windowWidth / 2) - 10, round((3/(abs(endInterval - startInterval))) * windowHeight), (windowWidth / 2) + 10, round((3/(abs(endInterval - startInterval))) * windowHeight));
	line((windowWidth / 2) - 10, round((1/(abs(endInterval - startInterval))) * windowHeight), (windowWidth / 2) + 10, round((1/(abs(endInterval - startInterval))) * windowHeight));
	noStroke();
	
	//opis osi
	textSize(20);
	fill(0);
	text("1", round((3/(abs(endInterval - startInterval))) * windowWidth) - 5, (windowHeight / 2) + 30);
	text("-1", round((1/(abs(endInterval - startInterval))) * windowWidth) - 9, (windowHeight / 2) + 30);
	text("-1", (windowWidth / 2) - 30, round((1/(abs(endInterval - startInterval))) * windowHeight) + 8);
	text("1", (windowWidth / 2) - 22, round((3/(abs(endInterval - startInterval))) * windowHeight) + 8);
	textSize(15);

	for (var i = 0; i < fireFlays.length; i++)
		fireFlays[i].show(0, 0, false);	
	
	fireFlays[best].show(0, 0, true);

	for (var i = 0; i < fireFlays.length; i++) {		
		for (var j = 0; j < fireFlays.length; j++) {
			if (fireFlays[j].glow > fireFlays[i].glow) {
				var dist = distance(fireFlays[i].x, fireFlays[j].x, fireFlays[i].y, fireFlays[j].y);
				fireFlays[i].x += exp((- 1 / 4) * sq(dist)) * (fireFlays[j].x - fireFlays[i].x) + random(-step, step);
				fireFlays[i].y += exp((- 1 / 4) * sq(dist)) * (fireFlays[j].y - fireFlays[i].y) + random(-step, step); 
			}
		}
		
		fireFlays[i].calcGlow(0, 0);

		if (fireFlays[i].glow > bestglow) {
			best = i;
			bestglow = fireFlays[i].glow;
			bestx = fireFlays[i].x;
			besty = fireFlays[i].y;
		}
	}

	fireFlays[best].x += random(-step, step);
	fireFlays[best].y += random(-step, step);
	fireFlays[best].calcGlow(0, 0);

	if (step > 0.001) step -= 0.001;
	else if (step > 0.0001) step -= 0.00001;
	else if (step > 0.00001) step -= 0.000001;
	else if (step > 0.000001) step -= 0.0000001;
	else if (step > 0.0000001) step -= 0.00000001;
	else if (step > 0.00000001) step -= 0.000000001;
	else if (step > 0.000000001) step -= 0.0000000001;

	fill(0, 0, 0);
	text(iterations + " \nx" + bestx + "\ny" + besty + " \nf(x) " + bestglow.toPrecision(21), 10, 30);
	iterations++;
}