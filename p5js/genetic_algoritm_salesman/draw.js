var population = [];
var nodes = [];
var populationQuantity;
var best, bestPhen;
var bestIndyvidual = [];
var phenotyps = [];
var mutationProbability;
var worst;

function setup() {
	createCanvas(windowWidth, windowHeight);
	frameRate(5);
	textSize(32);
	populationQuantity = 100;
	nodesQuantity = 10;
	mutationProbability = 0.1;

	generatePopulation(population, populationQuantity);

	for (var i = 0; i < nodesQuantity; ++i)
		nodes[i] = new Nodee(round(random(60, windowWidth - 60)), round(random(60, windowHeight - 60)));

	//console.log(population);
	//console.log(nodes);

	worst = 0;
	bestPhen = phenotyp(population[0]);
	bestIndyvidual = population[0].slice();
}

function generatePopulation(_population, n) {
	for (var i = 0; i < n; i++) {
		var _indyvidual = [];
		generateIndyvidual(_indyvidual, nodesQuantity);
		_population[i] = _indyvidual;
	}
}

function add(a, b) {
    return a + b;
}

function generateIndyvidual(_indyvidual, n) {
	for (var i = 0; i < n; ++i) {
		_indyvidual[i] = (-1);
	}

	for (var i = 0; i < n; ++i) {
		var done = true;
		var best = 0;
		do {
			var tmp = Math.floor(Math.random() * n)
			done = true;

			for (var j = 0; j < n; ++j) {
				if (_indyvidual[j] === tmp) {
					done = false;
					break;
				}
				else best = tmp;
			}

		} while (!done);

		_indyvidual[i] = best;
	} 
}

function phenotyp(_indyvidual) {
	var result = 0;

	for (var i = 0; i < _indyvidual.length; ++i) {
		result += dist(nodes[_indyvidual[i % _indyvidual.length]].x, nodes[_indyvidual[i % _indyvidual.length]].y, nodes[_indyvidual[(i + 1) % _indyvidual.length]].x, nodes[_indyvidual[(i + 1) % _indyvidual.length]].y);
	}

	return result;
}

function mutations(_population, probability) {
	for (var i = 0; i < _population.length; ++i) {
		for (var j = 0; j < _population[i].length; ++j) {
			var randomize = random();
			var mutation = floor(random(_population[i].length));
			if (randomize <= probability) {
				var tmp = _population[i][j];
				_population[i][j] = mutation;
				_population[i][_population[i].indexOf(mutation)] = tmp;
			}
		}
	}
}

function compare(a,b) {
    return a > b ? true : false;
}

function reverseCompare(a,b) {
    return a < b ? true : false;
}

function crossing(_population, _phenotyps) {
	var sum = _phenotyps.reduce(add, 0);
	var fitnessSum = 0;
	var fitness = [];
	var normalize = [];
	var pool = [];

	for (var i = 0; i < _phenotyps.length; ++i) {
		fitness[i] = 1 / (_phenotyps[i] + 1);
		fitnessSum += fitness[i];
	}

	normalize[0] = fitness[0] / fitnessSum;

	for (var i = 1; i < _phenotyps.length; ++i) {
		normalize[i] = normalize[i - 1] + (fitness[i] / fitnessSum);
	}

	//console.log(sum);
	//console.log(normalize);
	// losowanie puli rodziców
	for (var i = 0; i < _population.length * 2; ++i) {
		var shot = random();
		for (var j = 0; j < _phenotyps.length; ++j) {
			if (j != 0) {
				if ((shot < normalize[j]) && (shot >= normalize[j - 1])) {
					pool[i] = _population[j];
					break;
				}
			} else if (shot < normalize[0]) {
				pool[i] = _population[0];
				break;
			}
		}
	}
	//print(population);
	//print(normalize);
	//print(pool);
	//	
	for (var i = 0; i < _population.length; ++i) {
		var parent1 = pool[floor(random(pool.length))].slice();
		var parent2 = pool[floor(random(pool.length))].slice();
		var edgeArray = [];

		// tworzenie tablicy krawędzi
		for (var z = 0; z < parent1.length; ++z) {
			var tmpArray = [];
			var tmp = [];
			
			tmpArray[0] = parent1[(parent1.indexOf(z) - 1 + parent1.length) % parent1.length];
			tmpArray[1] = parent1[(parent1.indexOf(z) + 1) % parent1.length];
			tmpArray[2] = parent2[(parent2.indexOf(z) - 1 + parent2.length) % parent2.length];
			tmpArray[3] = parent2[(parent2.indexOf(z) + 1) % parent2.length];
			var done = false;
		
			for (var j = 0; j < tmpArray.length; ++j) {
				if (tmpArray[j] === null) continue;
				done = false;  
				
				for (var k = j + 1; k < tmpArray.length; ++k) {
					if (tmpArray[j] === tmpArray[k]) {
						tmp.push(new Edge(tmpArray[j], true));
						done = true;
						tmpArray[k] = null;
						break;
					}
				}
				if (!done) tmp.push(new Edge(tmpArray[j], false));
			}
			edgeArray[z] = tmp;
		}
		//print(edgeArray);
		var first = floor(random(edgeArray.length));
		var chilld = [];
		chilld[0] = first;

		var best = 5;
		var bestIndex = 0;
		var done = true;
	
		do {
			done = true;
			best = 5;
			bestIndex = -1;

			// usówanie wystepowania wierzchołka
			for (var j = 0; j < edgeArray.length; ++j) {
				var tmpK = -1;
				
				if (edgeArray[j] != null) {
					for (var k = 0; k < edgeArray[j].length; ++k)
						if (edgeArray[j][k].node === chilld[chilld.length - 1]) tmpK = k;
					if (tmpK != -1) edgeArray[j].splice(tmpK, 1);
				}
			}

			actualEdge = edgeArray[chilld[chilld.length - 1]];

			if (actualEdge.length != 0) {
				for (var j = 0; j < actualEdge.length; ++j) {
					var tmp = actualEdge[j];
				
					if (tmp.both) {
						bestIndex = tmp.node;
						break;
					}
					else if (edgeArray[tmp.node].length < best) {
						best = edgeArray[tmp.node].length;
						bestIndex = tmp.node;
					}
				}
			} else {
				for (var j = 0; j < edgeArray.length; ++j) {
					if ((edgeArray[j] != null) && (edgeArray[j] != actualEdge)) {
						bestIndex = j;
					}
				}
			}
			
			edgeArray[chilld[chilld.length - 1]] = null;

			if (bestIndex != -1)
				chilld.push(bestIndex);

			for (var j = 0; j < edgeArray.length; ++j)
				if (edgeArray[j] != null) done = false;
		} while (!done);

		_population[i] = chilld;
	}
}

function draw() {
	background(51);
	
	fill(255, 100, 0);
	stroke(255, 30);
	strokeWeight(3);
	//print(population);
        phenotyps = [];
	var actualBest = population[0].slice();
	var actualBestPhen = phenotyp(population[0]); 
	//	szukanie najlepszego
	for (var i = 0; i < population.length; ++i) {
		phenotyps[i] = phenotyp(population[i]);
		if (actualBestPhen > phenotyp(population[i])) {
			actualBestPhen = phenotyp(population[i]);
			actualBest = population[i].slice();
		}
		if (worst < phenotyp(population[i])) worst = i;
	}

	if (bestPhen > actualBestPhen) {
		bestPhen = actualBestPhen;
		bestIndyvidual = actualBest.slice();
		console.log(bestPhen);
	}
	
	//	wyświetlanie krawędzi
	//stroke(255, 30);
	//strokeWeight(3);
	//for (var i = 0; i < population.length; ++i) {
	//	for (var j = 0; j < nodes.length; ++j) {
	//		line(nodes[population[i][j]].x, nodes[population[i][j]].y, nodes[population[i][(j + 1) % nodes.length]].x, nodes[population[i][(j + 1) % nodes.length]].y);
	//	}
	//}

	//	wyświetlenie aktualnego najlepszego osobnika
	stroke(color(124,252,0), 1);
	strokeWeight(1);
	for (var i = 0; i < nodes.length; ++i)
		line(nodes[actualBest[i]].x, nodes[actualBest[i]].y, nodes[actualBest[(i + 1) % nodes.length]].x, nodes[actualBest[(i + 1) % nodes.length]].y);


	//	wyświetlanie ogólnie najlepszego osobnika
	stroke(color(255,0,239), 100);
	strokeWeight(5);
	for (var i = 0; i < nodes.length; ++i)
		line(nodes[bestIndyvidual[i]].x, nodes[bestIndyvidual[i]].y, nodes[bestIndyvidual[(i + 1) % nodes.length]].x, nodes[bestIndyvidual[(i + 1) % nodes.length]].y);

	// wyświetlanie węzłów
	noStroke();
	for (var i = 0; i < nodes.length; ++i)
		ellipse(nodes[i].x, nodes[i].y, 20, 20);

	crossing(population, phenotyps);
	mutations(population, mutationProbability);

	//text(percent, 10, 30);
	//test();
	//asd
}