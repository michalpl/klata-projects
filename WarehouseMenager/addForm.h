#pragma once

#include "DataHolders.h"

namespace projektojppodejscie2 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for addForm
	/// </summary>
	public ref class addForm : public System::Windows::Forms::Form
	{

	private: System::Windows::Forms::Form ^ otherform;
	
	public:
		addForm(void/*System::Windows::Forms::Form ^o*/)
		{
			//otherform = o;
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~addForm()
		{
			if (components)
			{
				delete components;
			}
		}
	public: DataHolders holder;
	public: System::Windows::Forms::TextBox^  itemsNameTextBox;
	private: System::Windows::Forms::ComboBox^  unitsComboBox;
	private: System::Windows::Forms::MaskedTextBox^  quantityMaskedTextBox;
	private: System::Windows::Forms::MaskedTextBox^  pricesMaskedTextBox;
	public: System::Windows::Forms::Button^  addItemsButton;
	private: System::Windows::Forms::Button^  cancelButton;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::GroupBox^  groupBox3;
	private: System::Windows::Forms::GroupBox^  groupBox4;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->itemsNameTextBox = (gcnew System::Windows::Forms::TextBox());
			this->unitsComboBox = (gcnew System::Windows::Forms::ComboBox());
			this->quantityMaskedTextBox = (gcnew System::Windows::Forms::MaskedTextBox());
			this->pricesMaskedTextBox = (gcnew System::Windows::Forms::MaskedTextBox());
			this->addItemsButton = (gcnew System::Windows::Forms::Button());
			this->cancelButton = (gcnew System::Windows::Forms::Button());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->SuspendLayout();
			// 
			// itemsNameTextBox
			// 
			this->itemsNameTextBox->Location = System::Drawing::Point(6, 19);
			this->itemsNameTextBox->Name = L"itemsNameTextBox";
			this->itemsNameTextBox->Size = System::Drawing::Size(219, 20);
			this->itemsNameTextBox->TabIndex = 0;
			// 
			// unitsComboBox
			// 
			this->unitsComboBox->FormattingEnabled = true;
			this->unitsComboBox->Items->AddRange(gcnew cli::array< System::Object^  >(6) { L"kg", L"szt", L"m", L"l", L"t", L"g" });
			this->unitsComboBox->Location = System::Drawing::Point(7, 17);
			this->unitsComboBox->Name = L"unitsComboBox";
			this->unitsComboBox->Size = System::Drawing::Size(88, 21);
			this->unitsComboBox->TabIndex = 1;
			// 
			// quantityMaskedTextBox
			// 
			this->quantityMaskedTextBox->Location = System::Drawing::Point(6, 19);
			this->quantityMaskedTextBox->Mask = L"00000000";
			this->quantityMaskedTextBox->Name = L"quantityMaskedTextBox";
			this->quantityMaskedTextBox->Size = System::Drawing::Size(112, 20);
			this->quantityMaskedTextBox->TabIndex = 3;
			this->quantityMaskedTextBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// pricesMaskedTextBox
			// 
			this->pricesMaskedTextBox->Location = System::Drawing::Point(6, 19);
			this->pricesMaskedTextBox->Mask = L"00000.00";
			this->pricesMaskedTextBox->Name = L"pricesMaskedTextBox";
			this->pricesMaskedTextBox->Size = System::Drawing::Size(112, 20);
			this->pricesMaskedTextBox->TabIndex = 4;
			this->pricesMaskedTextBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// addItemsButton
			// 
			this->addItemsButton->Location = System::Drawing::Point(41, 167);
			this->addItemsButton->Name = L"addItemsButton";
			this->addItemsButton->Size = System::Drawing::Size(71, 34);
			this->addItemsButton->TabIndex = 5;
			this->addItemsButton->Text = L"Add";
			this->addItemsButton->UseVisualStyleBackColor = true;
			this->addItemsButton->Click += gcnew System::EventHandler(this, &addForm::addItemsButton_Click);
			// 
			// cancelButton
			// 
			this->cancelButton->Location = System::Drawing::Point(140, 167);
			this->cancelButton->Name = L"cancelButton";
			this->cancelButton->Size = System::Drawing::Size(64, 34);
			this->cancelButton->TabIndex = 6;
			this->cancelButton->Text = L"Cancel";
			this->cancelButton->UseVisualStyleBackColor = true;
			this->cancelButton->Click += gcnew System::EventHandler(this, &addForm::cancelButton_Click);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->itemsNameTextBox);
			this->groupBox1->Location = System::Drawing::Point(10, 12);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(231, 45);
			this->groupBox1->TabIndex = 7;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Thing name:";
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->quantityMaskedTextBox);
			this->groupBox2->Location = System::Drawing::Point(10, 63);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(124, 47);
			this->groupBox2->TabIndex = 8;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Number:";
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->unitsComboBox);
			this->groupBox3->Location = System::Drawing::Point(140, 63);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(101, 47);
			this->groupBox3->TabIndex = 9;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"Unit:";
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->pricesMaskedTextBox);
			this->groupBox4->Location = System::Drawing::Point(10, 116);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Size = System::Drawing::Size(124, 45);
			this->groupBox4->TabIndex = 10;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L"Price:";
			// 
			// addForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->ClientSize = System::Drawing::Size(251, 206);
			this->Controls->Add(this->groupBox4);
			this->Controls->Add(this->groupBox3);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->cancelButton);
			this->Controls->Add(this->addItemsButton);
			this->MaximizeBox = false;
			this->Name = L"addForm";
			this->Text = L"Dodawanie przedmiotu";
			this->Shown += gcnew System::EventHandler(this, &addForm::addForm_Shown);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->groupBox3->ResumeLayout(false);
			this->groupBox4->ResumeLayout(false);
			this->groupBox4->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
private: System::Void addItemsButton_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void cancelButton_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void addForm_Shown(System::Object^  sender, System::EventArgs^  e);
};
}
