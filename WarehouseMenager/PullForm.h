#pragma once
#include "DataHolders.h"

namespace projektojppodejscie2 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for PullForm
	/// </summary>
	public ref class PullForm : public System::Windows::Forms::Form
	{
	public:
		PullForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~PullForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^  nameTextBox;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::MaskedTextBox^  numberMaxkedTextBox;
	private: System::Windows::Forms::Button^  okButton;
	private: System::Windows::Forms::Button^  cancelButton;

	private: Int32 number;
	public: DataHolders holder;

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->nameTextBox = (gcnew System::Windows::Forms::TextBox());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->numberMaxkedTextBox = (gcnew System::Windows::Forms::MaskedTextBox());
			this->okButton = (gcnew System::Windows::Forms::Button());
			this->cancelButton = (gcnew System::Windows::Forms::Button());
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->SuspendLayout();
			// 
			// nameTextBox
			// 
			this->nameTextBox->Location = System::Drawing::Point(7, 19);
			this->nameTextBox->Name = L"nameTextBox";
			this->nameTextBox->ReadOnly = true;
			this->nameTextBox->Size = System::Drawing::Size(185, 20);
			this->nameTextBox->TabIndex = 0;
			this->nameTextBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->nameTextBox);
			this->groupBox1->Location = System::Drawing::Point(12, 12);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(200, 48);
			this->groupBox1->TabIndex = 1;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Name:";
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->numberMaxkedTextBox);
			this->groupBox2->Location = System::Drawing::Point(12, 66);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(200, 48);
			this->groupBox2->TabIndex = 2;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Quantity:";
			// 
			// numberMaxkedTextBox
			// 
			this->numberMaxkedTextBox->Location = System::Drawing::Point(7, 19);
			this->numberMaxkedTextBox->Mask = L"000000000";
			this->numberMaxkedTextBox->Name = L"numberMaxkedTextBox";
			this->numberMaxkedTextBox->Size = System::Drawing::Size(185, 20);
			this->numberMaxkedTextBox->TabIndex = 0;
			this->numberMaxkedTextBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// okButton
			// 
			this->okButton->Location = System::Drawing::Point(12, 120);
			this->okButton->Name = L"okButton";
			this->okButton->Size = System::Drawing::Size(95, 23);
			this->okButton->TabIndex = 3;
			this->okButton->Text = L"OK";
			this->okButton->UseVisualStyleBackColor = true;
			this->okButton->Click += gcnew System::EventHandler(this, &PullForm::okButton_Click);
			// 
			// cancelButton
			// 
			this->cancelButton->Location = System::Drawing::Point(117, 120);
			this->cancelButton->Name = L"cancelButton";
			this->cancelButton->Size = System::Drawing::Size(95, 23);
			this->cancelButton->TabIndex = 4;
			this->cancelButton->Text = L"Cancel";
			this->cancelButton->UseVisualStyleBackColor = true;
			this->cancelButton->Click += gcnew System::EventHandler(this, &PullForm::cancelButton_Click);
			// 
			// PullForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(222, 154);
			this->Controls->Add(this->cancelButton);
			this->Controls->Add(this->okButton);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Name = L"PullForm";
			this->Text = L"PullForm";
			this->Shown += gcnew System::EventHandler(this, &PullForm::PullForm_Shown);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
private: System::Void PullForm_Shown(System::Object^  sender, System::EventArgs^  e) {
		this->nameTextBox->Text = holder.GetItemName();
		this->number = Int32::Parse(this->holder.GetNumbers());
		this->numberMaxkedTextBox->Select();
	}
private: System::Void okButton_Click(System::Object^  sender, System::EventArgs^  e) {
	if (this->number >= Int32::Parse(this->numberMaxkedTextBox->Text))
	{
		this->holder.SetNumbers(this->numberMaxkedTextBox->Text);
		this->Close();
	}
	else
	{
		MessageBox::Show("Chcesz wybra� wi�cej ni� jest produktu!", "Error:");
	}
}
private: System::Void cancelButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	this->holder.SetNumbers("0");
	this->Close();
}
};
}
