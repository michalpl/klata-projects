#pragma once

#include "DataHolders.h"

namespace projektojppodejscie2 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for EditForm
	/// </summary>
	public ref class EditForm : public System::Windows::Forms::Form
	{
	public:
		EditForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~EditForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^  nameTextBox;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::MaskedTextBox^  numberMaskedTextBox;
	private: System::Windows::Forms::GroupBox^  groupBox4;
	private: System::Windows::Forms::ComboBox^  unitsComboBox;
	private: System::Windows::Forms::GroupBox^  groupBox3;

	private: System::Windows::Forms::Button^  okButton;
	private: System::Windows::Forms::Button^  cancelButton;
	
	public: DataHolders holder;
	private: System::Windows::Forms::TextBox^  priceTextBox;
	public:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->nameTextBox = (gcnew System::Windows::Forms::TextBox());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->numberMaskedTextBox = (gcnew System::Windows::Forms::MaskedTextBox());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->unitsComboBox = (gcnew System::Windows::Forms::ComboBox());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->priceTextBox = (gcnew System::Windows::Forms::TextBox());
			this->okButton = (gcnew System::Windows::Forms::Button());
			this->cancelButton = (gcnew System::Windows::Forms::Button());
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->SuspendLayout();
			// 
			// nameTextBox
			// 
			this->nameTextBox->Location = System::Drawing::Point(6, 19);
			this->nameTextBox->Name = L"nameTextBox";
			this->nameTextBox->Size = System::Drawing::Size(188, 20);
			this->nameTextBox->TabIndex = 0;
			this->nameTextBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->nameTextBox);
			this->groupBox1->Location = System::Drawing::Point(12, 12);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(200, 46);
			this->groupBox1->TabIndex = 1;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Name:";
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->numberMaskedTextBox);
			this->groupBox2->Location = System::Drawing::Point(12, 65);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(200, 46);
			this->groupBox2->TabIndex = 2;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Quantity:";
			// 
			// numberMaskedTextBox
			// 
			this->numberMaskedTextBox->Location = System::Drawing::Point(6, 19);
			this->numberMaskedTextBox->Mask = L"00000000";
			this->numberMaskedTextBox->Name = L"numberMaskedTextBox";
			this->numberMaskedTextBox->Size = System::Drawing::Size(188, 20);
			this->numberMaskedTextBox->TabIndex = 0;
			this->numberMaskedTextBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->unitsComboBox);
			this->groupBox4->Location = System::Drawing::Point(12, 117);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Size = System::Drawing::Size(200, 47);
			this->groupBox4->TabIndex = 10;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L"Unit:";
			// 
			// unitsComboBox
			// 
			this->unitsComboBox->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->unitsComboBox->FormattingEnabled = true;
			this->unitsComboBox->Items->AddRange(gcnew cli::array< System::Object^  >(6) { L"kg", L"szt", L"m", L"l", L"t", L"g" });
			this->unitsComboBox->Location = System::Drawing::Point(6, 17);
			this->unitsComboBox->Name = L"unitsComboBox";
			this->unitsComboBox->RightToLeft = System::Windows::Forms::RightToLeft::Yes;
			this->unitsComboBox->Size = System::Drawing::Size(188, 21);
			this->unitsComboBox->TabIndex = 1;
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->priceTextBox);
			this->groupBox3->Location = System::Drawing::Point(12, 170);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(200, 45);
			this->groupBox3->TabIndex = 11;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"Price:";
			// 
			// priceTextBox
			// 
			this->priceTextBox->Location = System::Drawing::Point(6, 20);
			this->priceTextBox->Name = L"priceTextBox";
			this->priceTextBox->Size = System::Drawing::Size(188, 20);
			this->priceTextBox->TabIndex = 0;
			this->priceTextBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// okButton
			// 
			this->okButton->Location = System::Drawing::Point(12, 226);
			this->okButton->Name = L"okButton";
			this->okButton->Size = System::Drawing::Size(95, 23);
			this->okButton->TabIndex = 12;
			this->okButton->Text = L"OK";
			this->okButton->UseVisualStyleBackColor = true;
			this->okButton->Click += gcnew System::EventHandler(this, &EditForm::okButton_Click);
			// 
			// cancelButton
			// 
			this->cancelButton->Location = System::Drawing::Point(117, 226);
			this->cancelButton->Name = L"cancelButton";
			this->cancelButton->Size = System::Drawing::Size(95, 23);
			this->cancelButton->TabIndex = 13;
			this->cancelButton->Text = L"Cancel";
			this->cancelButton->UseVisualStyleBackColor = true;
			this->cancelButton->Click += gcnew System::EventHandler(this, &EditForm::cancelButton_Click);
			// 
			// EditForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(226, 259);
			this->Controls->Add(this->cancelButton);
			this->Controls->Add(this->okButton);
			this->Controls->Add(this->groupBox3);
			this->Controls->Add(this->groupBox4);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Name = L"EditForm";
			this->Text = L"EditForm";
			this->Shown += gcnew System::EventHandler(this, &EditForm::EditForm_Shown);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->groupBox4->ResumeLayout(false);
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
private: System::Void EditForm_Shown(System::Object^  sender, System::EventArgs^  e) 
{
	this->nameTextBox->Text = this->holder.GetItemName();
	this->numberMaskedTextBox->Text = this->holder.GetNumbers();
	this->unitsComboBox->Text = this->holder.GetUnit();
	this->priceTextBox->Text = this->holder.GetPrice();
}
private: System::Void okButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	if (!String::IsNullOrWhiteSpace(this->nameTextBox->Text) && !String::IsNullOrWhiteSpace(this->numberMaskedTextBox->Text)
		&& !String::IsNullOrWhiteSpace(this->unitsComboBox->Text) && !String::IsNullOrWhiteSpace(this->priceTextBox->Text))
	{
		this->holder.SetItemName(this->nameTextBox->Text);
		this->holder.SetNumbers(this->numberMaskedTextBox->Text->Replace(" ", ""));
		this->holder.SetUnit(this->unitsComboBox->Text);
		this->holder.SetPrice(this->priceTextBox->Text->Replace(" ", ""));

		this->Close();
	}
	else MessageBox::Show("Nie wszystko zosta�o wype�nione!", "Error:");
}
private: System::Void cancelButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	this->holder.SetEmpty();
	this->Close();
}
};
}
