#include "settingsDataHolder.h"

void settingsDataHolder::setAlerts(System::Boolean _alerts)
{
	this->alerts = _alerts;
}

void settingsDataHolder::setYellowCode(System::String^ _yellowCode)
{
	this->yellowCode = _yellowCode;
}

void settingsDataHolder::setRedCode(System::String^ _redCode)
{
	this->redCode = _redCode;
}

System::Boolean settingsDataHolder::getStatus(void)
{
	return this->alerts;
}

System::String^ settingsDataHolder::getYellowCode(void)
{
	return this->yellowCode;
}

System::String^ settingsDataHolder::getRedCode(void)
{
	return this->redCode;
}

settingsDataHolder::settingsDataHolder()
{
}
