#include "addForm.h"

System::Void projektojppodejscie2::addForm::addItemsButton_Click(System::Object ^ sender, System::EventArgs ^ e)
{
	if (!String::IsNullOrWhiteSpace(this->itemsNameTextBox->Text) && !String::IsNullOrWhiteSpace(this->quantityMaskedTextBox->Text)
		&& !String::IsNullOrWhiteSpace(this->unitsComboBox->Text) && !String::IsNullOrWhiteSpace(this->pricesMaskedTextBox->Text))
	{
		this->holder.SetItemName(this->itemsNameTextBox->Text);
		this->holder.SetNumbers(this->quantityMaskedTextBox->Text->Replace(" ", ""));
		this->holder.SetUnit(this->unitsComboBox->Text);
		this->holder.SetPrice(this->pricesMaskedTextBox->Text->Replace(" ", ""));

		this->itemsNameTextBox->Text = "";
		this->quantityMaskedTextBox->Text = "";
		this->unitsComboBox->Text = "";
		this->unitsComboBox->Text = "";
		this->pricesMaskedTextBox->Text = "";

		this->Close();
	}
	else
	{
		MessageBox::Show("Prosze wype�ni� pola!", "Error:");
	}
}

System::Void projektojppodejscie2::addForm::cancelButton_Click(System::Object^  sender, System::EventArgs^  e)
{
	this->holder.SetEmpty();

	this->itemsNameTextBox->Text = "";
	this->quantityMaskedTextBox->Text = "";
	this->unitsComboBox->Text = "";
	this->unitsComboBox->Text = "";
	this->pricesMaskedTextBox->Text = "";

	this->Close();
}

System::Void projektojppodejscie2::addForm::addForm_Shown(System::Object ^ sender, System::EventArgs ^ e)
{
	this->holder.SetEmpty();
	this->unitsComboBox->SelectedIndex = 0;
	this->addItemsButton->Select();
}
