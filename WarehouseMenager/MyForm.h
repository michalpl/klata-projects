#pragma once

#include "addForm.h"
#include "pushForm.h"
#include "PullForm.h"
#include "EditForm.h"
#include "SettingsForm.h"
#include "currentDateFormat.h"

namespace projektojppodejscie2 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}

	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Button^  addButton;
	private: System::Windows::Forms::Button^  pullButton;
	private: System::Windows::Forms::TabPage^  warehous1;
	private: System::Windows::Forms::DataGridView^  dataGridView1;
	private: System::Windows::Forms::TabControl^  tabControl1;
	private: System::Windows::Forms::TabPage^  warehous2;
	private: System::Windows::Forms::Button^  deleteButton;
	private: System::Windows::Forms::Button^  pushButton;
	private: System::Windows::Forms::Button^  settingsButton;
	private: System::Windows::Forms::Button^  editButton;






	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Button^  saveButton;
	private: System::Windows::Forms::Button^  loadButton;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  addNewToolStripMenuItem;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator1;
	private: System::Windows::Forms::ToolStripMenuItem^  editToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  deleteToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  saveToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  loadToolStripMenuItem;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator2;
	private: System::ComponentModel::IContainer^  components;

	private: Boolean alerts = true;
	private: Int16 yellowCode = 20;
	private: Int16 redCode = 10;
	private: String^ fileName;
	private: System::Windows::Forms::Button^  closeButton;
	private: System::Windows::Forms::ToolStripMenuItem^  closeToolStripMenuItem;
	private: System::Windows::Forms::ListBox^  tipsListBox;
	private: System::Windows::Forms::Button^  saveAsButton;
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  saveToolStripMenuItem1;
	private: System::Windows::Forms::ToolStripMenuItem^  saveAsStripMenuItem;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator3;
	private: System::Windows::Forms::ToolStripMenuItem^  loadToolStripMenuItem1;
	private: System::Windows::Forms::ToolStripMenuItem^  closeFileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  editToolStripMenuItem1;
	private: System::Windows::Forms::ToolStripMenuItem^  editToolStripMenuItem2;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator4;
	private: System::Windows::Forms::ToolStripMenuItem^  pushToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  pullToolStripMenuItem;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator5;
	private: System::Windows::Forms::ToolStripMenuItem^  deleteDataToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  settingsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  helpToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  aboutToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  viewHelpToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  addNewDataToolStripMenuItem;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  itemsFromDataGridPage1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  numbersFromDataGridPage2;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  unitsFromDataGridPage1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  pricesFromDataGridPage1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  DateFromDataGridPage1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dateEditFromDataGridPage1;

	private: Boolean fileIsOpen = false;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->addButton = (gcnew System::Windows::Forms::Button());
			this->pullButton = (gcnew System::Windows::Forms::Button());
			this->warehous1 = (gcnew System::Windows::Forms::TabPage());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->itemsFromDataGridPage1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->numbersFromDataGridPage2 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->unitsFromDataGridPage1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->pricesFromDataGridPage1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->DateFromDataGridPage1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->dateEditFromDataGridPage1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->contextMenuStrip1 = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
			this->addNewToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->editToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->deleteToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator2 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->saveToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->loadToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->closeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->warehous2 = (gcnew System::Windows::Forms::TabPage());
			this->deleteButton = (gcnew System::Windows::Forms::Button());
			this->pushButton = (gcnew System::Windows::Forms::Button());
			this->settingsButton = (gcnew System::Windows::Forms::Button());
			this->editButton = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->saveButton = (gcnew System::Windows::Forms::Button());
			this->loadButton = (gcnew System::Windows::Forms::Button());
			this->closeButton = (gcnew System::Windows::Forms::Button());
			this->tipsListBox = (gcnew System::Windows::Forms::ListBox());
			this->saveAsButton = (gcnew System::Windows::Forms::Button());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->saveToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->saveAsStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator3 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->loadToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->closeFileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->editToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->editToolStripMenuItem2 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator4 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->pushToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->pullToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator5 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->addNewDataToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->deleteDataToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->settingsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->helpToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->viewHelpToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->aboutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->warehous1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->contextMenuStrip1->SuspendLayout();
			this->tabControl1->SuspendLayout();
			this->menuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(576, 32);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(244, 20);
			this->textBox1->TabIndex = 1;
			this->textBox1->TextChanged += gcnew System::EventHandler(this, &MyForm::textBox1_TextChanged);
			// 
			// addButton
			// 
			this->addButton->Location = System::Drawing::Point(576, 58);
			this->addButton->Name = L"addButton";
			this->addButton->Size = System::Drawing::Size(57, 21);
			this->addButton->TabIndex = 2;
			this->addButton->Text = L"Add new";
			this->addButton->UseVisualStyleBackColor = true;
			this->addButton->Click += gcnew System::EventHandler(this, &MyForm::addButton_Click);
			// 
			// pullButton
			// 
			this->pullButton->Location = System::Drawing::Point(69, 32);
			this->pullButton->Name = L"pullButton";
			this->pullButton->Size = System::Drawing::Size(55, 48);
			this->pullButton->TabIndex = 3;
			this->pullButton->Text = L"Pull";
			this->pullButton->UseVisualStyleBackColor = true;
			this->pullButton->Click += gcnew System::EventHandler(this, &MyForm::pullButton_Click);
			// 
			// warehous1
			// 
			this->warehous1->AutoScroll = true;
			this->warehous1->Controls->Add(this->dataGridView1);
			this->warehous1->Location = System::Drawing::Point(4, 22);
			this->warehous1->Name = L"warehous1";
			this->warehous1->Padding = System::Windows::Forms::Padding(3);
			this->warehous1->Size = System::Drawing::Size(822, 514);
			this->warehous1->TabIndex = 0;
			this->warehous1->Text = L"Warehous1";
			this->warehous1->UseVisualStyleBackColor = true;
			// 
			// dataGridView1
			// 
			this->dataGridView1->BackgroundColor = System::Drawing::SystemColors::Control;
			this->dataGridView1->CausesValidation = false;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
			this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(6) {
				this->itemsFromDataGridPage1,
					this->numbersFromDataGridPage2, this->unitsFromDataGridPage1, this->pricesFromDataGridPage1, this->DateFromDataGridPage1, this->dateEditFromDataGridPage1
			});
			this->dataGridView1->ContextMenuStrip = this->contextMenuStrip1;
			this->dataGridView1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->dataGridView1->Location = System::Drawing::Point(3, 3);
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			this->dataGridView1->RowHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::None;
			this->dataGridView1->RowTemplate->ReadOnly = true;
			this->dataGridView1->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->dataGridView1->Size = System::Drawing::Size(816, 508);
			this->dataGridView1->TabIndex = 0;
			this->dataGridView1->Click += gcnew System::EventHandler(this, &MyForm::dataGridView1_Click);
			this->dataGridView1->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::dataGridView1_MouseDown);
			// 
			// itemsFromDataGridPage1
			// 
			this->itemsFromDataGridPage1->HeaderText = L"Items";
			this->itemsFromDataGridPage1->Name = L"itemsFromDataGridPage1";
			this->itemsFromDataGridPage1->ReadOnly = true;
			this->itemsFromDataGridPage1->Width = 280;
			// 
			// numbersFromDataGridPage2
			// 
			this->numbersFromDataGridPage2->HeaderText = L"Quantity";
			this->numbersFromDataGridPage2->Name = L"numbersFromDataGridPage2";
			this->numbersFromDataGridPage2->ReadOnly = true;
			// 
			// unitsFromDataGridPage1
			// 
			this->unitsFromDataGridPage1->HeaderText = L"Units";
			this->unitsFromDataGridPage1->Name = L"unitsFromDataGridPage1";
			this->unitsFromDataGridPage1->ReadOnly = true;
			// 
			// pricesFromDataGridPage1
			// 
			this->pricesFromDataGridPage1->HeaderText = L"Prices";
			this->pricesFromDataGridPage1->Name = L"pricesFromDataGridPage1";
			this->pricesFromDataGridPage1->ReadOnly = true;
			// 
			// DateFromDataGridPage1
			// 
			this->DateFromDataGridPage1->HeaderText = L"Date added ";
			this->DateFromDataGridPage1->Name = L"DateFromDataGridPage1";
			this->DateFromDataGridPage1->ReadOnly = true;
			// 
			// dateEditFromDataGridPage1
			// 
			this->dateEditFromDataGridPage1->HeaderText = L"Date Edit";
			this->dateEditFromDataGridPage1->Name = L"dateEditFromDataGridPage1";
			this->dateEditFromDataGridPage1->ReadOnly = true;
			// 
			// contextMenuStrip1
			// 
			this->contextMenuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(8) {
				this->addNewToolStripMenuItem,
					this->toolStripSeparator1, this->editToolStripMenuItem, this->deleteToolStripMenuItem, this->toolStripSeparator2, this->saveToolStripMenuItem,
					this->loadToolStripMenuItem, this->closeToolStripMenuItem
			});
			this->contextMenuStrip1->Name = L"contextMenuStrip1";
			this->contextMenuStrip1->Size = System::Drawing::Size(122, 148);
			// 
			// addNewToolStripMenuItem
			// 
			this->addNewToolStripMenuItem->Name = L"addNewToolStripMenuItem";
			this->addNewToolStripMenuItem->Size = System::Drawing::Size(121, 22);
			this->addNewToolStripMenuItem->Text = L"Add new";
			this->addNewToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::addButton_Click);
			// 
			// toolStripSeparator1
			// 
			this->toolStripSeparator1->Name = L"toolStripSeparator1";
			this->toolStripSeparator1->Size = System::Drawing::Size(118, 6);
			// 
			// editToolStripMenuItem
			// 
			this->editToolStripMenuItem->Name = L"editToolStripMenuItem";
			this->editToolStripMenuItem->Size = System::Drawing::Size(121, 22);
			this->editToolStripMenuItem->Text = L"Edit";
			this->editToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::editButton_Click);
			// 
			// deleteToolStripMenuItem
			// 
			this->deleteToolStripMenuItem->Name = L"deleteToolStripMenuItem";
			this->deleteToolStripMenuItem->Size = System::Drawing::Size(121, 22);
			this->deleteToolStripMenuItem->Text = L"Delete";
			this->deleteToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::deleteButton_Click);
			// 
			// toolStripSeparator2
			// 
			this->toolStripSeparator2->Name = L"toolStripSeparator2";
			this->toolStripSeparator2->Size = System::Drawing::Size(118, 6);
			// 
			// saveToolStripMenuItem
			// 
			this->saveToolStripMenuItem->Name = L"saveToolStripMenuItem";
			this->saveToolStripMenuItem->Size = System::Drawing::Size(121, 22);
			this->saveToolStripMenuItem->Text = L"Save";
			this->saveToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::saveButton_Click);
			// 
			// loadToolStripMenuItem
			// 
			this->loadToolStripMenuItem->Name = L"loadToolStripMenuItem";
			this->loadToolStripMenuItem->Size = System::Drawing::Size(121, 22);
			this->loadToolStripMenuItem->Text = L"Load";
			this->loadToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::loadButton_Click);
			// 
			// closeToolStripMenuItem
			// 
			this->closeToolStripMenuItem->Name = L"closeToolStripMenuItem";
			this->closeToolStripMenuItem->Size = System::Drawing::Size(121, 22);
			this->closeToolStripMenuItem->Text = L"Close";
			this->closeToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::closeButton_Click);
			// 
			// tabControl1
			// 
			this->tabControl1->Controls->Add(this->warehous1);
			this->tabControl1->Controls->Add(this->warehous2);
			this->tabControl1->Location = System::Drawing::Point(0, 86);
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->Size = System::Drawing::Size(830, 540);
			this->tabControl1->TabIndex = 0;
			// 
			// warehous2
			// 
			this->warehous2->Location = System::Drawing::Point(4, 22);
			this->warehous2->Name = L"warehous2";
			this->warehous2->Padding = System::Windows::Forms::Padding(3);
			this->warehous2->Size = System::Drawing::Size(822, 514);
			this->warehous2->TabIndex = 1;
			this->warehous2->Text = L"Warehous2";
			this->warehous2->UseVisualStyleBackColor = true;
			// 
			// deleteButton
			// 
			this->deleteButton->Location = System::Drawing::Point(639, 58);
			this->deleteButton->Name = L"deleteButton";
			this->deleteButton->Size = System::Drawing::Size(55, 21);
			this->deleteButton->TabIndex = 4;
			this->deleteButton->Text = L"Delete";
			this->deleteButton->UseVisualStyleBackColor = true;
			this->deleteButton->Click += gcnew System::EventHandler(this, &MyForm::deleteButton_Click);
			// 
			// pushButton
			// 
			this->pushButton->Location = System::Drawing::Point(8, 32);
			this->pushButton->Name = L"pushButton";
			this->pushButton->Size = System::Drawing::Size(55, 48);
			this->pushButton->TabIndex = 5;
			this->pushButton->Text = L"Push";
			this->pushButton->UseVisualStyleBackColor = true;
			this->pushButton->Click += gcnew System::EventHandler(this, &MyForm::pushButton_Click);
			// 
			// settingsButton
			// 
			this->settingsButton->Location = System::Drawing::Point(763, 58);
			this->settingsButton->Name = L"settingsButton";
			this->settingsButton->Size = System::Drawing::Size(57, 21);
			this->settingsButton->TabIndex = 6;
			this->settingsButton->Text = L"Settings";
			this->settingsButton->UseVisualStyleBackColor = true;
			this->settingsButton->Click += gcnew System::EventHandler(this, &MyForm::settingsButton_Click);
			// 
			// editButton
			// 
			this->editButton->Location = System::Drawing::Point(700, 58);
			this->editButton->Name = L"editButton";
			this->editButton->Size = System::Drawing::Size(57, 21);
			this->editButton->TabIndex = 7;
			this->editButton->Text = L"Edit";
			this->editButton->UseVisualStyleBackColor = true;
			this->editButton->Click += gcnew System::EventHandler(this, &MyForm::editButton_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(523, 35);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(44, 13);
			this->label1->TabIndex = 8;
			this->label1->Text = L"Search:";
			// 
			// saveButton
			// 
			this->saveButton->Location = System::Drawing::Point(193, 33);
			this->saveButton->Name = L"saveButton";
			this->saveButton->Size = System::Drawing::Size(57, 21);
			this->saveButton->TabIndex = 9;
			this->saveButton->Text = L"Save";
			this->saveButton->UseVisualStyleBackColor = true;
			this->saveButton->Click += gcnew System::EventHandler(this, &MyForm::saveButton_Click);
			// 
			// loadButton
			// 
			this->loadButton->Location = System::Drawing::Point(130, 33);
			this->loadButton->Name = L"loadButton";
			this->loadButton->Size = System::Drawing::Size(57, 21);
			this->loadButton->TabIndex = 10;
			this->loadButton->Text = L"Load";
			this->loadButton->UseVisualStyleBackColor = true;
			this->loadButton->Click += gcnew System::EventHandler(this, &MyForm::loadButton_Click);
			// 
			// closeButton
			// 
			this->closeButton->Location = System::Drawing::Point(130, 58);
			this->closeButton->Name = L"closeButton";
			this->closeButton->Size = System::Drawing::Size(57, 22);
			this->closeButton->TabIndex = 11;
			this->closeButton->Text = L"Close file";
			this->closeButton->UseVisualStyleBackColor = true;
			this->closeButton->Click += gcnew System::EventHandler(this, &MyForm::closeButton_Click);
			// 
			// tipsListBox
			// 
			this->tipsListBox->FormattingEnabled = true;
			this->tipsListBox->Location = System::Drawing::Point(576, 51);
			this->tipsListBox->Name = L"tipsListBox";
			this->tipsListBox->Size = System::Drawing::Size(244, 95);
			this->tipsListBox->TabIndex = 1;
			this->tipsListBox->Click += gcnew System::EventHandler(this, &MyForm::tipsListBox_Click);
			// 
			// saveAsButton
			// 
			this->saveAsButton->Location = System::Drawing::Point(193, 58);
			this->saveAsButton->Name = L"saveAsButton";
			this->saveAsButton->Size = System::Drawing::Size(57, 22);
			this->saveAsButton->TabIndex = 12;
			this->saveAsButton->Text = L"Save as";
			this->saveAsButton->UseVisualStyleBackColor = true;
			this->saveAsButton->Click += gcnew System::EventHandler(this, &MyForm::saveAsButton_Click);
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {
				this->fileToolStripMenuItem,
					this->editToolStripMenuItem1, this->settingsToolStripMenuItem, this->helpToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(830, 24);
			this->menuStrip1->TabIndex = 13;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(5) {
				this->saveToolStripMenuItem1,
					this->saveAsStripMenuItem, this->toolStripSeparator3, this->loadToolStripMenuItem1, this->closeFileToolStripMenuItem
			});
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(37, 20);
			this->fileToolStripMenuItem->Text = L"File";
			this->fileToolStripMenuItem->DropDownOpening += gcnew System::EventHandler(this, &MyForm::fileToolStripMenuItem_DropDownOpening);
			// 
			// saveToolStripMenuItem1
			// 
			this->saveToolStripMenuItem1->Name = L"saveToolStripMenuItem1";
			this->saveToolStripMenuItem1->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::S));
			this->saveToolStripMenuItem1->Size = System::Drawing::Size(152, 22);
			this->saveToolStripMenuItem1->Text = L"Save";
			this->saveToolStripMenuItem1->Click += gcnew System::EventHandler(this, &MyForm::saveButton_Click);
			// 
			// saveAsStripMenuItem
			// 
			this->saveAsStripMenuItem->Name = L"saveAsStripMenuItem";
			this->saveAsStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->saveAsStripMenuItem->Text = L"Save as...";
			this->saveAsStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::saveAsButton_Click);
			// 
			// toolStripSeparator3
			// 
			this->toolStripSeparator3->Name = L"toolStripSeparator3";
			this->toolStripSeparator3->Size = System::Drawing::Size(149, 6);
			// 
			// loadToolStripMenuItem1
			// 
			this->loadToolStripMenuItem1->Name = L"loadToolStripMenuItem1";
			this->loadToolStripMenuItem1->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::O));
			this->loadToolStripMenuItem1->Size = System::Drawing::Size(152, 22);
			this->loadToolStripMenuItem1->Text = L"Open";
			this->loadToolStripMenuItem1->Click += gcnew System::EventHandler(this, &MyForm::loadButton_Click);
			// 
			// closeFileToolStripMenuItem
			// 
			this->closeFileToolStripMenuItem->Name = L"closeFileToolStripMenuItem";
			this->closeFileToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->closeFileToolStripMenuItem->Text = L"Close file";
			this->closeFileToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::closeButton_Click);
			// 
			// editToolStripMenuItem1
			// 
			this->editToolStripMenuItem1->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(7) {
				this->editToolStripMenuItem2,
					this->toolStripSeparator4, this->pushToolStripMenuItem, this->pullToolStripMenuItem, this->toolStripSeparator5, this->addNewDataToolStripMenuItem,
					this->deleteDataToolStripMenuItem
			});
			this->editToolStripMenuItem1->Name = L"editToolStripMenuItem1";
			this->editToolStripMenuItem1->Size = System::Drawing::Size(39, 20);
			this->editToolStripMenuItem1->Text = L"Edit";
			this->editToolStripMenuItem1->DropDownOpening += gcnew System::EventHandler(this, &MyForm::editToolStripMenuItem1_DropDownOpening);
			// 
			// editToolStripMenuItem2
			// 
			this->editToolStripMenuItem2->Name = L"editToolStripMenuItem2";
			this->editToolStripMenuItem2->Size = System::Drawing::Size(187, 22);
			this->editToolStripMenuItem2->Text = L"Edit";
			this->editToolStripMenuItem2->Click += gcnew System::EventHandler(this, &MyForm::editButton_Click);
			// 
			// toolStripSeparator4
			// 
			this->toolStripSeparator4->Name = L"toolStripSeparator4";
			this->toolStripSeparator4->Size = System::Drawing::Size(184, 6);
			// 
			// pushToolStripMenuItem
			// 
			this->pushToolStripMenuItem->Name = L"pushToolStripMenuItem";
			this->pushToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::Oemplus));
			this->pushToolStripMenuItem->Size = System::Drawing::Size(187, 22);
			this->pushToolStripMenuItem->Text = L"Push";
			this->pushToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::pushButton_Click);
			// 
			// pullToolStripMenuItem
			// 
			this->pullToolStripMenuItem->Name = L"pullToolStripMenuItem";
			this->pullToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::OemMinus));
			this->pullToolStripMenuItem->Size = System::Drawing::Size(187, 22);
			this->pullToolStripMenuItem->Text = L"Pull";
			this->pullToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::pullButton_Click);
			// 
			// toolStripSeparator5
			// 
			this->toolStripSeparator5->Name = L"toolStripSeparator5";
			this->toolStripSeparator5->Size = System::Drawing::Size(184, 6);
			// 
			// addNewDataToolStripMenuItem
			// 
			this->addNewDataToolStripMenuItem->Name = L"addNewDataToolStripMenuItem";
			this->addNewDataToolStripMenuItem->Size = System::Drawing::Size(187, 22);
			this->addNewDataToolStripMenuItem->Text = L"Add new data";
			this->addNewDataToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::addButton_Click);
			// 
			// deleteDataToolStripMenuItem
			// 
			this->deleteDataToolStripMenuItem->Name = L"deleteDataToolStripMenuItem";
			this->deleteDataToolStripMenuItem->ShortcutKeys = System::Windows::Forms::Keys::Delete;
			this->deleteDataToolStripMenuItem->Size = System::Drawing::Size(187, 22);
			this->deleteDataToolStripMenuItem->Text = L"Delete data";
			this->deleteDataToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::deleteButton_Click);
			// 
			// settingsToolStripMenuItem
			// 
			this->settingsToolStripMenuItem->Name = L"settingsToolStripMenuItem";
			this->settingsToolStripMenuItem->Size = System::Drawing::Size(61, 20);
			this->settingsToolStripMenuItem->Text = L"Settings";
			this->settingsToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::settingsButton_Click);
			// 
			// helpToolStripMenuItem
			// 
			this->helpToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->viewHelpToolStripMenuItem,
					this->aboutToolStripMenuItem
			});
			this->helpToolStripMenuItem->Name = L"helpToolStripMenuItem";
			this->helpToolStripMenuItem->Size = System::Drawing::Size(44, 20);
			this->helpToolStripMenuItem->Text = L"Help";
			// 
			// viewHelpToolStripMenuItem
			// 
			this->viewHelpToolStripMenuItem->Name = L"viewHelpToolStripMenuItem";
			this->viewHelpToolStripMenuItem->Size = System::Drawing::Size(127, 22);
			this->viewHelpToolStripMenuItem->Text = L"View Help";
			// 
			// aboutToolStripMenuItem
			// 
			this->aboutToolStripMenuItem->Name = L"aboutToolStripMenuItem";
			this->aboutToolStripMenuItem->Size = System::Drawing::Size(127, 22);
			this->aboutToolStripMenuItem->Text = L"About";
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->BackColor = System::Drawing::SystemColors::HotTrack;
			this->ClientSize = System::Drawing::Size(830, 627);
			this->Controls->Add(this->tipsListBox);
			this->Controls->Add(this->menuStrip1);
			this->Controls->Add(this->saveAsButton);
			this->Controls->Add(this->closeButton);
			this->Controls->Add(this->loadButton);
			this->Controls->Add(this->saveButton);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->editButton);
			this->Controls->Add(this->settingsButton);
			this->Controls->Add(this->pushButton);
			this->Controls->Add(this->deleteButton);
			this->Controls->Add(this->pullButton);
			this->Controls->Add(this->addButton);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->tabControl1);
			this->MainMenuStrip = this->menuStrip1;
			this->MaximizeBox = false;
			this->Name = L"MyForm";
			this->Text = L"Magazynier";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &MyForm::MyForm_FormClosing);
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			this->Shown += gcnew System::EventHandler(this, &MyForm::MyForm_Shown);
			this->Click += gcnew System::EventHandler(this, &MyForm::MyForm_Click);
			this->warehous1->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->contextMenuStrip1->ResumeLayout(false);
			this->tabControl1->ResumeLayout(false);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e) 
{
	this->dataGridView1->AllowUserToAddRows = false;
}

private: System::Void addButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	addForm ^fomForAddItem = gcnew addForm;
	
	fomForAddItem->ShowDialog();

	if (!String::IsNullOrWhiteSpace(fomForAddItem->holder.GetItemName()) && !String::IsNullOrWhiteSpace(fomForAddItem->holder.GetNumbers())
		&& !String::IsNullOrWhiteSpace(fomForAddItem->holder.GetUnit()) && !String::IsNullOrWhiteSpace(fomForAddItem->holder.GetPrice()))
	{
		this->dataGridView1->Rows->Add(fomForAddItem->holder.GetItemName(), fomForAddItem->holder.GetNumbers(), fomForAddItem->holder.GetUnit(), fomForAddItem->holder.GetPrice(), strDateFormat());
		fomForAddItem->holder.SetEmpty();
	}

	if (alerts)
	{
		Int32 number;
		for each(DataGridViewRow^ row in this->dataGridView1->Rows)
		{
			number = Int32::Parse(this->dataGridView1->Rows[row->Index]->Cells[1]->Value->ToString());
			if ((number <= yellowCode) && (number > redCode))
			{
				this->dataGridView1->Rows[row->Index]->DefaultCellStyle->BackColor = Color::Yellow;
			}
			else if (number <= redCode)
			{
				this->dataGridView1->Rows[row->Index]->DefaultCellStyle->BackColor = Color::Red;
			}
			else this->dataGridView1->Rows[row->Index]->DefaultCellStyle->BackColor = Color::White;
		}
	}
	else for each(DataGridViewRow^ row in this->dataGridView1->Rows)
	{
		this->dataGridView1->Rows[row->Index]->DefaultCellStyle->BackColor = Color::White;
	}
}

private: System::Void deleteButton_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (this->dataGridView1->SelectedRows->Count != 0)
	{
		if (MessageBox::Show("Na pewno?", "Potwierd�", MessageBoxButtons::YesNo, MessageBoxIcon::Asterisk) == System::Windows::Forms::DialogResult::Yes)
		{
			for each(DataGridViewRow^ row in this->dataGridView1->SelectedRows)
			{
				this->dataGridView1->Rows->Remove(row);
			}
		}
	}
	if (alerts)
	{
		Int32 number;
		for each(DataGridViewRow^ row in this->dataGridView1->Rows)
		{
			number = Int32::Parse(this->dataGridView1->Rows[row->Index]->Cells[1]->Value->ToString());
			if ((number <= yellowCode) && (number > redCode))
			{
				this->dataGridView1->Rows[row->Index]->DefaultCellStyle->BackColor = Color::Yellow;
			}
			else if (number <= redCode)
			{
				this->dataGridView1->Rows[row->Index]->DefaultCellStyle->BackColor = Color::Red;
			}
			else this->dataGridView1->Rows[row->Index]->DefaultCellStyle->BackColor = Color::White;
		}
	}
	else for each(DataGridViewRow^ row in this->dataGridView1->Rows)
	{
		this->dataGridView1->Rows[row->Index]->DefaultCellStyle->BackColor = Color::White;
	}
}

private: System::Void settingsButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	SettingsForm^ settings = gcnew SettingsForm;

	settings->settingsHolder.setAlerts(this->alerts);
	settings->settingsHolder.setYellowCode(this->yellowCode.ToString());
	settings->settingsHolder.setRedCode(this->redCode.ToString());

	settings->ShowDialog();

	this->alerts = settings->settingsHolder.getStatus();
	this->yellowCode = Int16::Parse(settings->settingsHolder.getYellowCode());
	this->redCode = Int16::Parse(settings->settingsHolder.getRedCode());
}

private: System::Void MyForm_Shown(System::Object^  sender, System::EventArgs^  e) 
{
	this->dataGridView1->ClearSelection();
	this->tipsListBox->Visible = false;
}

private: System::Void pushButton_Click(System::Object^  sender, System::EventArgs^  e) {

	pushForm^ push = gcnew pushForm;
	Int32 number;

	if (this->dataGridView1->SelectedRows->Count != 0)
	{
		push->holder.SetItemName(this->dataGridView1->Rows[this->dataGridView1->SelectedRows[0]->Index]->Cells[0]->Value->ToString());
		push->ShowDialog();

		number = Int32::Parse(this->dataGridView1->Rows[this->dataGridView1->SelectedRows[0]->Index]->Cells[1]->Value->ToString());
		
		if (push->holder.GetNumbers() != "") number += Int32::Parse(push->holder.GetNumbers());

		this->dataGridView1->Rows[this->dataGridView1->SelectedRows[0]->Index]->Cells[1]->Value = number.ToString();
		
		if (alerts)
		{
			if ((number <= yellowCode) && (number > redCode))
			{
				this->dataGridView1->SelectedRows[0]->DefaultCellStyle->BackColor = Color::Yellow;
			}
			else if (number <= redCode)
			{
				this->dataGridView1->SelectedRows[0]->DefaultCellStyle->BackColor = Color::Red;
			}
			else this->dataGridView1->SelectedRows[0]->DefaultCellStyle->BackColor = Color::White;
		}
		else for each(DataGridViewRow^ row in this->dataGridView1->Rows)
		{
			this->dataGridView1->Rows[row->Index]->DefaultCellStyle->BackColor = Color::White;
		}
	}
}

private: System::Void pullButton_Click(System::Object^  sender, System::EventArgs^  e) {
	PullForm^ pull = gcnew PullForm;
	Int32 number;

	if (this->dataGridView1->SelectedRows->Count != 0)
	{
		number = Int32::Parse(this->dataGridView1->Rows[this->dataGridView1->SelectedRows[0]->Index]->Cells[1]->Value->ToString());
		pull->holder.SetNumbers(number.ToString());
		pull->holder.SetItemName(this->dataGridView1->Rows[this->dataGridView1->SelectedRows[0]->Index]->Cells[0]->Value->ToString());
		pull->ShowDialog();

		if (pull->holder.GetNumbers() != "") number -= Int32::Parse(pull->holder.GetNumbers());

		this->dataGridView1->Rows[this->dataGridView1->SelectedRows[0]->Index]->Cells[1]->Value = number.ToString();

		if (alerts)
		{
			if ((number <= yellowCode) && (number > redCode))
			{
				this->dataGridView1->SelectedRows[0]->DefaultCellStyle->BackColor = Color::Yellow;
			}
			else if (number <= redCode)
			{
				this->dataGridView1->SelectedRows[0]->DefaultCellStyle->BackColor = Color::Red;
			}
			else this->dataGridView1->SelectedRows[0]->DefaultCellStyle->BackColor = Color::White;
		}
		else for each(DataGridViewRow^ row in this->dataGridView1->Rows)
		{
			this->dataGridView1->Rows[row->Index]->DefaultCellStyle->BackColor = Color::White;
		}
	}
}
private: System::Void editButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	EditForm^ edit = gcnew EditForm;
	Int32 number;
	Double price;

	if (this->dataGridView1->SelectedRows->Count != 0)
	{
		edit->holder.SetItemName(this->dataGridView1->Rows[this->dataGridView1->SelectedRows[0]->Index]->Cells[0]->Value->ToString());
		edit->holder.SetNumbers(this->dataGridView1->Rows[this->dataGridView1->SelectedRows[0]->Index]->Cells[1]->Value->ToString());
		edit->holder.SetUnit(this->dataGridView1->Rows[this->dataGridView1->SelectedRows[0]->Index]->Cells[2]->Value->ToString());
		edit->holder.SetPrice(this->dataGridView1->Rows[this->dataGridView1->SelectedRows[0]->Index]->Cells[3]->Value->ToString());

		edit->ShowDialog();

		if (!String::IsNullOrWhiteSpace(edit->holder.GetItemName()) && !String::IsNullOrWhiteSpace(edit->holder.GetNumbers())
			&& !String::IsNullOrWhiteSpace(edit->holder.GetUnit()) && !String::IsNullOrWhiteSpace(edit->holder.GetPrice()))
		{
			this->dataGridView1->Rows[this->dataGridView1->SelectedRows[0]->Index]->Cells[0]->Value = edit->holder.GetItemName();
			this->dataGridView1->Rows[this->dataGridView1->SelectedRows[0]->Index]->Cells[1]->Value = edit->holder.GetNumbers();
			this->dataGridView1->Rows[this->dataGridView1->SelectedRows[0]->Index]->Cells[2]->Value = edit->holder.GetUnit();
			this->dataGridView1->Rows[this->dataGridView1->SelectedRows[0]->Index]->Cells[3]->Value = edit->holder.GetPrice();
			this->dataGridView1->Rows[this->dataGridView1->SelectedRows[0]->Index]->Cells[5]->Value = strDateFormat();
			number = Int32::Parse(edit->holder.GetNumbers());
		}

		if (alerts)
		{
			if ((number <= yellowCode) && (number > redCode))
			{
				this->dataGridView1->SelectedRows[0]->DefaultCellStyle->BackColor = Color::Yellow;
			}
			else if (number <= redCode)
			{
				this->dataGridView1->SelectedRows[0]->DefaultCellStyle->BackColor = Color::Red;
			}
			else this->dataGridView1->SelectedRows[0]->DefaultCellStyle->BackColor = Color::White;
		}
		else for each(DataGridViewRow^ row in this->dataGridView1->Rows)
		{
			this->dataGridView1->Rows[row->Index]->DefaultCellStyle->BackColor = Color::White;
		}
	}
}

private: System::Void dataGridView1_Click(System::Object^  sender, System::EventArgs^  e) {
	if (alerts)
	{
		//Doda� to do funkcji jak si� dowiem jak
		Int32 number;
		for each(DataGridViewRow^ row in this->dataGridView1->Rows)
		{
			number = Int32::Parse(this->dataGridView1->Rows[row->Index]->Cells[1]->Value->ToString());
			if ((number <= yellowCode) && (number > redCode))
			{
				this->dataGridView1->Rows[row->Index]->DefaultCellStyle->BackColor = Color::Yellow;
			}
			else if (number <= redCode)
			{
				this->dataGridView1->Rows[row->Index]->DefaultCellStyle->BackColor = Color::Red;
			}
			else this->dataGridView1->Rows[row->Index]->DefaultCellStyle->BackColor = Color::White;
		}
	}
	else for each(DataGridViewRow^ row in this->dataGridView1->Rows)
	{
		this->dataGridView1->Rows[row->Index]->DefaultCellStyle->BackColor = Color::White;
	}

	this->tipsListBox->Visible = false;
	this->textBox1->Clear();
}
private: System::Void saveButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	if ((this->fileIsOpen) && (File::Exists(fileName)))
	{
		StreamWriter^ writeFile = gcnew StreamWriter(fileName);

		for each(DataGridViewRow^ row in this->dataGridView1->Rows)
		{
			String^ editDate;

			if (this->dataGridView1->Rows[row->Index]->Cells[5]->Value == nullptr)
			{
				editDate = "";
			}
			else editDate = this->dataGridView1->Rows[row->Index]->Cells[5]->Value->ToString();

			writeFile->WriteLine(
				this->dataGridView1->Rows[row->Index]->Cells[0]->Value->ToString() + ";" +
				this->dataGridView1->Rows[row->Index]->Cells[1]->Value->ToString() + ";" +
				this->dataGridView1->Rows[row->Index]->Cells[2]->Value->ToString() + ";" +
				this->dataGridView1->Rows[row->Index]->Cells[3]->Value->ToString() + ";" +
				this->dataGridView1->Rows[row->Index]->Cells[4]->Value->ToString() + ";" +
				editDate
			);
		}
		writeFile->Close();
	}
	else if (this->dataGridView1->Rows->Count != 0)
	{
		SaveFileDialog^ fileDialog = gcnew SaveFileDialog();
		
		fileDialog->Filter = "Warehous data file|*.wdf";

		if (fileDialog->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			try
			{
				StreamWriter^ writeFile = gcnew StreamWriter(fileDialog->FileName);
				
				for each(DataGridViewRow^ row in this->dataGridView1->Rows)
				{
					String^ editDate;

					if (this->dataGridView1->Rows[row->Index]->Cells[5]->Value == nullptr)
					{
						editDate = "";
					}
					else editDate = this->dataGridView1->Rows[row->Index]->Cells[5]->Value->ToString();

					writeFile->WriteLine(
						this->dataGridView1->Rows[row->Index]->Cells[0]->Value->ToString() + ";" +
						this->dataGridView1->Rows[row->Index]->Cells[1]->Value->ToString() + ";" +
						this->dataGridView1->Rows[row->Index]->Cells[2]->Value->ToString() + ";" +
						this->dataGridView1->Rows[row->Index]->Cells[3]->Value->ToString() + ";" +
						this->dataGridView1->Rows[row->Index]->Cells[4]->Value->ToString() + ";" +
						editDate
					);
				}

				writeFile->Close();
			}
			catch (Exception^ err)
			{
				MessageBox::Show(err->ToString(), "Error:", MessageBoxButtons::OK, MessageBoxIcon::Error);
			}
			MessageBox::Show("Zapisano w " + fileDialog->FileName->ToString(), "Zapisano!", MessageBoxButtons::OK, MessageBoxIcon::Information);
		}
	}
}

private: System::Void loadButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	if (!this->fileIsOpen)
	{
		OpenFileDialog^ fileDialog = gcnew OpenFileDialog();

		fileDialog->Filter = "Warehous data file |*.wdf";

		if (fileDialog->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			try
			{
				StreamReader^ readFile = File::OpenText(fileDialog->FileName);
				String^ strLine;

				this->dataGridView1->Rows->Clear();
				this->dataGridView1->Refresh();

				while ((strLine = readFile->ReadLine()) != nullptr)
				{
					array<String ^>^ words;
					words = strLine->Split(';');

					this->dataGridView1->Rows->Add(words[0], words[1], words[2], words[3], words[4], words[5]);
				}

				readFile->Close();

				this->fileIsOpen = true;
				this->fileName = fileDialog->FileName;
				this->loadButton->Enabled = false;
			}
			catch (Exception^ e)
			{
				MessageBox::Show(e->ToString(), "Error:", MessageBoxButtons::OK, MessageBoxIcon::Error);
			}
		}
	}
}

private: System::Void dataGridView1_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	switch (e->Button)
	{
	case System::Windows::Forms::MouseButtons::Right:
		if (this->dataGridView1->SelectedRows->Count == 0)
		{
			this->editToolStripMenuItem->Enabled = false;
			this->deleteToolStripMenuItem->Enabled = false;
			this->saveToolStripMenuItem->Enabled = false;
		}
		else
		{
			this->editToolStripMenuItem->Enabled = true;
			this->deleteToolStripMenuItem->Enabled = true;
			this->saveToolStripMenuItem->Enabled = true;
		}

		if (this->fileIsOpen)
		{
			this->closeToolStripMenuItem->Enabled = true;
			this->loadToolStripMenuItem->Enabled = false;
		}
		else
		{
			this->closeToolStripMenuItem->Enabled = false;
			this->loadToolStripMenuItem->Enabled = true;
		}

		break;
	default:
		break;
	}
}
private: System::Void MyForm_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) 
{
	if (this->fileIsOpen)
	{
		if (MessageBox::Show("Zapisa� zmiany w " + this->fileName + "?", "Zmiany", MessageBoxButtons::YesNo, MessageBoxIcon::Question) ==
			System::Windows::Forms::DialogResult::Yes)
		{
			StreamWriter^ writeFile = gcnew StreamWriter(fileName);

			for each(DataGridViewRow^ row in this->dataGridView1->Rows)
			{
				String^ editDate;

				if (this->dataGridView1->Rows[row->Index]->Cells[5]->Value == nullptr)
				{
					editDate = "";
				}
				else editDate = this->dataGridView1->Rows[row->Index]->Cells[5]->Value->ToString();

				writeFile->WriteLine(
					this->dataGridView1->Rows[row->Index]->Cells[0]->Value->ToString() + ";" +
					this->dataGridView1->Rows[row->Index]->Cells[1]->Value->ToString() + ";" +
					this->dataGridView1->Rows[row->Index]->Cells[2]->Value->ToString() + ";" +
					this->dataGridView1->Rows[row->Index]->Cells[3]->Value->ToString() + ";" +
					this->dataGridView1->Rows[row->Index]->Cells[4]->Value->ToString() + ";" +
					editDate
				);
			}

			writeFile->Close();
		}
	}
	else if (this->dataGridView1->Rows->Count != 0)
	{
		if (MessageBox::Show("Zapisa� plik? ", "Zapis", MessageBoxButtons::YesNo, MessageBoxIcon::Question) ==
			System::Windows::Forms::DialogResult::Yes)
		{
			SaveFileDialog^ fileDialog = gcnew SaveFileDialog();

			fileDialog->Filter = "Warehous data file|*.wdf";

			if (fileDialog->ShowDialog() == System::Windows::Forms::DialogResult::OK)
			{
				try
				{
					StreamWriter^ writeFile = gcnew StreamWriter(fileDialog->FileName);

					for each(DataGridViewRow^ row in this->dataGridView1->Rows)
					{
						String^ editDate;

						if (this->dataGridView1->Rows[row->Index]->Cells[5]->Value == nullptr)
						{
							editDate = "";
						}
						else editDate = this->dataGridView1->Rows[row->Index]->Cells[5]->Value->ToString();

						writeFile->WriteLine(
							this->dataGridView1->Rows[row->Index]->Cells[0]->Value->ToString() + ";" +
							this->dataGridView1->Rows[row->Index]->Cells[1]->Value->ToString() + ";" +
							this->dataGridView1->Rows[row->Index]->Cells[2]->Value->ToString() + ";" +
							this->dataGridView1->Rows[row->Index]->Cells[3]->Value->ToString() + ";" +
							this->dataGridView1->Rows[row->Index]->Cells[4]->Value->ToString() + ";" +
							editDate
						);
					}

					writeFile->Close();
				}
				catch (Exception^ err)
				{
					MessageBox::Show(err->ToString(), "Error:", MessageBoxButtons::OK, MessageBoxIcon::Error);
				}
				MessageBox::Show("Zapisano w " + fileDialog->FileName->ToString(), "Zapisano!", MessageBoxButtons::OK, MessageBoxIcon::Information);
			}
		}
	}
}
private: System::Void closeButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	if (this->fileIsOpen)
	{
		if (MessageBox::Show("Zapisa� zmiany w " + this->fileName + "?", "Zmiany", MessageBoxButtons::YesNo, MessageBoxIcon::Question) ==
			System::Windows::Forms::DialogResult::Yes)
		{
			StreamWriter^ writeFile = gcnew StreamWriter(fileName);

			for each(DataGridViewRow^ row in this->dataGridView1->Rows)
			{
				String^ editDate;

				if (this->dataGridView1->Rows[row->Index]->Cells[5]->Value == nullptr)
				{
					editDate = "";
				}
				else editDate = this->dataGridView1->Rows[row->Index]->Cells[5]->Value->ToString();

				writeFile->WriteLine(
					this->dataGridView1->Rows[row->Index]->Cells[0]->Value->ToString() + ";" +
					this->dataGridView1->Rows[row->Index]->Cells[1]->Value->ToString() + ";" +
					this->dataGridView1->Rows[row->Index]->Cells[2]->Value->ToString() + ";" +
					this->dataGridView1->Rows[row->Index]->Cells[3]->Value->ToString() + ";" +
					this->dataGridView1->Rows[row->Index]->Cells[4]->Value->ToString() + ";" +
					editDate
				);
			}

			writeFile->Close();

			dataGridView1->Rows->Clear();
			this->fileIsOpen = false;
			this->loadButton->Enabled = true;
		}
		else
		{
			dataGridView1->Rows->Clear();
			this->fileIsOpen = false;
			this->loadButton->Enabled = true;
		}
	}
}
private: System::Void textBox1_TextChanged(System::Object^  sender, System::EventArgs^  e) 
{
	this->tipsListBox->Visible = true;

	this->tipsListBox->Items->Clear();

	for each(DataGridViewRow^ row in this->dataGridView1->Rows)
	{
		if (this->dataGridView1->Rows[row->Index]->Cells[0]->Value->ToString()->Contains(this->textBox1->Text) != 0)
		{
			this->tipsListBox->Items->Add(this->dataGridView1->Rows[row->Index]->Cells[0]->Value->ToString());
		}
	}
}
private: System::Void MyForm_Click(System::Object^  sender, System::EventArgs^  e) 
{
	this->tipsListBox->Visible = false;
	this->textBox1->Clear();
	this->dataGridView1->ClearSelection();
}
private: System::Void tipsListBox_Click(System::Object^  sender, System::EventArgs^  e) 
{
	if ((this->tipsListBox->Items->Count != 0) && (this->tipsListBox->SelectedItem != nullptr))
	{
		for each(DataGridViewRow^ row in this->dataGridView1->Rows)
		{
			if (this->dataGridView1->Rows[row->Index]->Cells[0]->Value->ToString() ==
				this->tipsListBox->SelectedItem->ToString())
			{
				this->dataGridView1->ClearSelection();
				this->dataGridView1->Rows[row->Index]->Selected = true;
				this->dataGridView1->CurrentCell = this->dataGridView1->Rows[row->Index]->Cells[0];

				this->textBox1->Clear();
				this->tipsListBox->Visible = false;
				return;
			}
		}
	}

	this->textBox1->Clear();
	this->tipsListBox->Visible = false;
}
private: System::Void saveAsButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	SaveFileDialog^ fileDialog = gcnew SaveFileDialog();

	fileDialog->Filter = "Warehous data file|*.wdf";

	if (this->dataGridView1->SelectedRows->Count != 0)
	{
		if (fileDialog->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			try
			{
				StreamWriter^ writeFile = gcnew StreamWriter(fileDialog->FileName);

				for each(DataGridViewRow^ row in this->dataGridView1->Rows)
				{
					String^ editDate;

					if (this->dataGridView1->Rows[row->Index]->Cells[5]->Value == nullptr)
					{
						editDate = "";
					}
					else editDate = this->dataGridView1->Rows[row->Index]->Cells[5]->Value->ToString();

					writeFile->WriteLine(
						this->dataGridView1->Rows[row->Index]->Cells[0]->Value->ToString() + ";" +
						this->dataGridView1->Rows[row->Index]->Cells[1]->Value->ToString() + ";" +
						this->dataGridView1->Rows[row->Index]->Cells[2]->Value->ToString() + ";" +
						this->dataGridView1->Rows[row->Index]->Cells[3]->Value->ToString() + ";" +
						this->dataGridView1->Rows[row->Index]->Cells[4]->Value->ToString() + ";" +
						editDate
					);
				}

				writeFile->Close();
			}
			catch (Exception^ err)
			{
				MessageBox::Show(err->ToString(), "Error:", MessageBoxButtons::OK, MessageBoxIcon::Error);
			}
			MessageBox::Show("Zapisano w " + fileDialog->FileName->ToString(), "Zapisano!", MessageBoxButtons::OK, MessageBoxIcon::Information);
		}
	}
	else MessageBox::Show("Brak danych", "Error:", MessageBoxButtons::OK, MessageBoxIcon::Error);
}
private: System::Void editToolStripMenuItem1_DropDownOpening(System::Object^  sender, System::EventArgs^  e) 
{
	if (this->dataGridView1->SelectedRows->Count == 0)
	{
		this->editToolStripMenuItem2->Enabled = false;
		this->pullToolStripMenuItem->Enabled = false;
		this->pushToolStripMenuItem->Enabled = false;
		this->deleteDataToolStripMenuItem->Enabled = false;
	}
	else
	{
		this->editToolStripMenuItem2->Enabled = true;
		this->pullToolStripMenuItem->Enabled = true;
		this->pushToolStripMenuItem->Enabled = true;
		this->deleteDataToolStripMenuItem->Enabled = true;
	}

}
private: System::Void fileToolStripMenuItem_DropDownOpening(System::Object^  sender, System::EventArgs^  e) 
{
	if (fileIsOpen)
	{
		this->loadToolStripMenuItem1->Enabled = false;
		this->closeFileToolStripMenuItem->Enabled = true;
	}
	else
	{
		this->loadToolStripMenuItem1->Enabled = true;
		this->closeFileToolStripMenuItem->Enabled = false;
	}

}
};
}
