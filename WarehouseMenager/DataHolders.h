#pragma once
ref class DataHolders
{
private:
	System::String^ itemName;
	System::String^ numbers;
	System::String^ unit;
	System::String^ price;

public:
	void SetItemName(System::String^ _itemName);
	void SetNumbers(System::String^ _numbers);
	void SetUnit(System::String^ _unit);
	void SetPrice(System::String^ _price);

	void SetEmpty(void);

	System::String^ GetItemName(void);
	System::String^ GetNumbers(void);
	System::String^ GetUnit(void);
	System::String^ GetPrice(void);

	DataHolders();
};

