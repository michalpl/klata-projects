#pragma once

#include "settingsDataHolder.h"

namespace projektojppodejscie2 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for SettingsForm
	/// </summary>
	public ref class SettingsForm : public System::Windows::Forms::Form
	{
	public:
		SettingsForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~SettingsForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MaskedTextBox^  yellowAlertMaskedCheckBox;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::Label^  redAlertLabel;
	private: System::Windows::Forms::Label^  yellowAletLabel;
	private: System::Windows::Forms::MaskedTextBox^  redAlertMaskedCheckBox;
	private: System::Windows::Forms::CheckBox^  alertsCheckBox;
	private: System::Windows::Forms::Button^  saveButton;
	private: System::Windows::Forms::Button^  cancelButton;

	public: settingsDataHolder settingsHolder;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->yellowAlertMaskedCheckBox = (gcnew System::Windows::Forms::MaskedTextBox());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->redAlertLabel = (gcnew System::Windows::Forms::Label());
			this->yellowAletLabel = (gcnew System::Windows::Forms::Label());
			this->redAlertMaskedCheckBox = (gcnew System::Windows::Forms::MaskedTextBox());
			this->alertsCheckBox = (gcnew System::Windows::Forms::CheckBox());
			this->saveButton = (gcnew System::Windows::Forms::Button());
			this->cancelButton = (gcnew System::Windows::Forms::Button());
			this->groupBox1->SuspendLayout();
			this->SuspendLayout();
			// 
			// yellowAlertMaskedCheckBox
			// 
			this->yellowAlertMaskedCheckBox->Location = System::Drawing::Point(94, 19);
			this->yellowAlertMaskedCheckBox->Name = L"yellowAlertMaskedCheckBox";
			this->yellowAlertMaskedCheckBox->Size = System::Drawing::Size(100, 20);
			this->yellowAlertMaskedCheckBox->TabIndex = 0;
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->redAlertLabel);
			this->groupBox1->Controls->Add(this->yellowAletLabel);
			this->groupBox1->Controls->Add(this->redAlertMaskedCheckBox);
			this->groupBox1->Controls->Add(this->yellowAlertMaskedCheckBox);
			this->groupBox1->Location = System::Drawing::Point(13, 36);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(200, 78);
			this->groupBox1->TabIndex = 1;
			this->groupBox1->TabStop = false;
			// 
			// redAlertLabel
			// 
			this->redAlertLabel->AutoSize = true;
			this->redAlertLabel->Location = System::Drawing::Point(7, 51);
			this->redAlertLabel->Name = L"redAlertLabel";
			this->redAlertLabel->Size = System::Drawing::Size(53, 13);
			this->redAlertLabel->TabIndex = 3;
			this->redAlertLabel->Text = L"Red alert:";
			// 
			// yellowAletLabel
			// 
			this->yellowAletLabel->AutoSize = true;
			this->yellowAletLabel->Location = System::Drawing::Point(6, 22);
			this->yellowAletLabel->Name = L"yellowAletLabel";
			this->yellowAletLabel->Size = System::Drawing::Size(64, 13);
			this->yellowAletLabel->TabIndex = 2;
			this->yellowAletLabel->Text = L"Yellow alert:";
			// 
			// redAlertMaskedCheckBox
			// 
			this->redAlertMaskedCheckBox->Location = System::Drawing::Point(94, 45);
			this->redAlertMaskedCheckBox->Name = L"redAlertMaskedCheckBox";
			this->redAlertMaskedCheckBox->Size = System::Drawing::Size(100, 20);
			this->redAlertMaskedCheckBox->TabIndex = 1;
			// 
			// alertsCheckBox
			// 
			this->alertsCheckBox->AutoSize = true;
			this->alertsCheckBox->Location = System::Drawing::Point(13, 12);
			this->alertsCheckBox->Name = L"alertsCheckBox";
			this->alertsCheckBox->RightToLeft = System::Windows::Forms::RightToLeft::Yes;
			this->alertsCheckBox->Size = System::Drawing::Size(52, 17);
			this->alertsCheckBox->TabIndex = 2;
			this->alertsCheckBox->Text = L"Alerts";
			this->alertsCheckBox->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->alertsCheckBox->UseVisualStyleBackColor = true;
			this->alertsCheckBox->CheckStateChanged += gcnew System::EventHandler(this, &SettingsForm::alertsCheckBox_CheckStateChanged);
			// 
			// saveButton
			// 
			this->saveButton->Location = System::Drawing::Point(13, 125);
			this->saveButton->Name = L"saveButton";
			this->saveButton->Size = System::Drawing::Size(92, 23);
			this->saveButton->TabIndex = 3;
			this->saveButton->Text = L"Save";
			this->saveButton->UseVisualStyleBackColor = true;
			this->saveButton->Click += gcnew System::EventHandler(this, &SettingsForm::saveButton_Click);
			// 
			// cancelButton
			// 
			this->cancelButton->Location = System::Drawing::Point(121, 125);
			this->cancelButton->Name = L"cancelButton";
			this->cancelButton->Size = System::Drawing::Size(92, 23);
			this->cancelButton->TabIndex = 4;
			this->cancelButton->Text = L"Cancel";
			this->cancelButton->UseVisualStyleBackColor = true;
			this->cancelButton->Click += gcnew System::EventHandler(this, &SettingsForm::cancelButton_Click);
			// 
			// SettingsForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(225, 158);
			this->Controls->Add(this->cancelButton);
			this->Controls->Add(this->saveButton);
			this->Controls->Add(this->alertsCheckBox);
			this->Controls->Add(this->groupBox1);
			this->Name = L"SettingsForm";
			this->Text = L"SettingsForm";
			this->Shown += gcnew System::EventHandler(this, &SettingsForm::SettingsForm_Shown);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

private: System::Void alertsCheckBox_CheckStateChanged(System::Object^  sender, System::EventArgs^  e) 
{
	if (this->alertsCheckBox->Checked)
	{
		this->groupBox1->Enabled = true;
		this->settingsHolder.setAlerts(true);
	}
	else
	{
		this->groupBox1->Enabled = false;
		this->settingsHolder.setAlerts(false);
	}
}
private: System::Void SettingsForm_Shown(System::Object^  sender, System::EventArgs^  e) 
{
	this->groupBox1->Enabled = this->settingsHolder.getStatus();
	this->alertsCheckBox->Checked = this->settingsHolder.getStatus();
	this->yellowAlertMaskedCheckBox->Text = this->settingsHolder.getYellowCode();
	this->redAlertMaskedCheckBox->Text = this->settingsHolder.getRedCode();
}
private: System::Void saveButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	Int16 yellowCode = Int16::Parse(this->yellowAlertMaskedCheckBox->Text->Replace(" ", ""));
	Int16 redCode = Int16::Parse(this->redAlertMaskedCheckBox->Text->Replace(" ", ""));

	if (redCode < yellowCode)
	{
		if (this->alertsCheckBox->Checked)
		{
			this->settingsHolder.setAlerts(true);
			this->settingsHolder.setRedCode(this->redAlertMaskedCheckBox->Text->Replace(" ", ""));
			this->settingsHolder.setYellowCode(this->yellowAlertMaskedCheckBox->Text->Replace(" ", ""));
		}
		else this->settingsHolder.setAlerts(false);

		this->Close();
	}
	else MessageBox::Show("Czerwony musi by� mniejszy od ��tego!", "Error:");
}
private: System::Void cancelButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	this->Close();
}
};
}
