#include "DataHolders.h"

void DataHolders::SetItemName(System::String^ _itemName)
{
	itemName = _itemName;
}

void DataHolders::SetNumbers(System::String^ _numbers)
{
	this->numbers = _numbers;
}

void DataHolders::SetUnit(System::String^ _unit)
{
	this->unit = _unit;
}

void DataHolders::SetPrice(System::String^ _price)
{
	this->price = _price;
}

void DataHolders::SetEmpty(void)
{
	this->itemName = "";
	this->numbers = "";
	this->unit = "";
	this->price = "";
}

System::String^ DataHolders::GetItemName(void)
{
	return this->itemName;
}

System::String^ DataHolders::GetNumbers(void)
{
	return this->numbers;
}

System::String^ DataHolders::GetUnit(void)
{
	return this->unit;
}

System::String^ DataHolders::GetPrice(void)
{
	return this->price;
}

DataHolders::DataHolders()
{
	this->itemName = "";
	this->numbers = "";
	this->unit = "";
	this->price = "";
}
