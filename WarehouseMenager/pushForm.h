#pragma once
#include "DataHolders.h"

namespace projektojppodejscie2 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for pushForm
	/// </summary>
	public ref class pushForm : public System::Windows::Forms::Form
	{
	public:
		pushForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~pushForm()
		{
			if (components)
			{
				delete components;
			}
		}

	private: System::Windows::Forms::MaskedTextBox^  numberMaskedTextBox;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::Button^  okButton;
	private: System::Windows::Forms::Button^  cancelButton;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::TextBox^  nameTextBox;

	public: DataHolders holder;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->numberMaskedTextBox = (gcnew System::Windows::Forms::MaskedTextBox());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->okButton = (gcnew System::Windows::Forms::Button());
			this->cancelButton = (gcnew System::Windows::Forms::Button());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->nameTextBox = (gcnew System::Windows::Forms::TextBox());
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->SuspendLayout();
			// 
			// numberMaskedTextBox
			// 
			this->numberMaskedTextBox->Location = System::Drawing::Point(6, 20);
			this->numberMaskedTextBox->Mask = L"00000000";
			this->numberMaskedTextBox->Name = L"numberMaskedTextBox";
			this->numberMaskedTextBox->Size = System::Drawing::Size(188, 20);
			this->numberMaskedTextBox->TabIndex = 0;
			this->numberMaskedTextBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->numberMaskedTextBox);
			this->groupBox1->Location = System::Drawing::Point(12, 70);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(200, 46);
			this->groupBox1->TabIndex = 1;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Quantity:";
			// 
			// okButton
			// 
			this->okButton->Location = System::Drawing::Point(12, 122);
			this->okButton->Name = L"okButton";
			this->okButton->Size = System::Drawing::Size(93, 23);
			this->okButton->TabIndex = 2;
			this->okButton->Text = L"OK";
			this->okButton->UseVisualStyleBackColor = true;
			this->okButton->Click += gcnew System::EventHandler(this, &pushForm::okButton_Click);
			// 
			// cancelButton
			// 
			this->cancelButton->Location = System::Drawing::Point(119, 122);
			this->cancelButton->Name = L"cancelButton";
			this->cancelButton->Size = System::Drawing::Size(93, 23);
			this->cancelButton->TabIndex = 3;
			this->cancelButton->Text = L"Cancel";
			this->cancelButton->UseVisualStyleBackColor = true;
			this->cancelButton->Click += gcnew System::EventHandler(this, &pushForm::cancelButton_Click);
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->nameTextBox);
			this->groupBox2->Location = System::Drawing::Point(12, 13);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(200, 51);
			this->groupBox2->TabIndex = 4;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Name:";
			// 
			// nameTextBox
			// 
			this->nameTextBox->Location = System::Drawing::Point(6, 19);
			this->nameTextBox->Name = L"nameTextBox";
			this->nameTextBox->ReadOnly = true;
			this->nameTextBox->Size = System::Drawing::Size(188, 20);
			this->nameTextBox->TabIndex = 0;
			this->nameTextBox->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// pushForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->ClientSize = System::Drawing::Size(224, 157);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->cancelButton);
			this->Controls->Add(this->okButton);
			this->Controls->Add(this->groupBox1);
			this->Name = L"pushForm";
			this->Text = L"pushForm";
			this->Shown += gcnew System::EventHandler(this, &pushForm::pushForm_Shown);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
private: System::Void pushForm_Shown(System::Object^  sender, System::EventArgs^  e) {
		this->nameTextBox->Text = this->holder.GetItemName();
		this->numberMaskedTextBox->Select();
	}
private: System::Void okButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	if (!String::IsNullOrWhiteSpace(this->numberMaskedTextBox->Text))
		this->holder.SetNumbers(this->numberMaskedTextBox->Text);
	else this->holder.SetNumbers("0");

	this->Close();
}
private: System::Void cancelButton_Click(System::Object^  sender, System::EventArgs^  e) 
{
	this->holder.SetNumbers("0");
	this->Close();
}
};
}
