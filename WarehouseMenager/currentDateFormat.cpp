#include "currentDateFormat.h"

System::String ^ strDateFormat()
{
	System::DateTime^ nowDate = System::DateTime::Now;

	System::String^ currentData = "";

	if (nowDate->Day.ToString()->Length == 1)
	{
		currentData += "0";
	}

	currentData += nowDate->Day;
	currentData += "/";

	if (nowDate->Month.ToString()->Length == 1)
	{
		currentData += "0";
	}

	currentData += nowDate->Month;
	currentData += "/";
	currentData += nowDate->Year;

	return currentData;
}
