#pragma once
ref class settingsDataHolder
{
private:
	System::Boolean alerts;
	System::String^ yellowCode;
	System::String^ redCode;

public:
	void setAlerts(System::Boolean _alerts);
	void setYellowCode(System::String^ _yellowCode);
	void setRedCode(System::String^ _redCode);

	System::Boolean getStatus(void);
	System::String^ getYellowCode(void);
	System::String^ getRedCode(void);
	
	settingsDataHolder();
};

