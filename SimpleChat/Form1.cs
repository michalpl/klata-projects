﻿using System;
using System.Windows.Forms;

namespace CSharp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            ChatForm chatWindow = new ChatForm(this);
            chatWindow.Show();

            groupBox1.Enabled = false;
            groupBox2.Enabled = false;
            startButton.Enabled = false;

            this.Hide();
        }

        private void cancleButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }
    }
}
