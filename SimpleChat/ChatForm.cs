﻿using System;

using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;

namespace CSharp
{
    public partial class ChatForm : Form
    {
        public ChatForm(Form1 includingForm)
        {
            firstForm = includingForm; 
            InitializeComponent();
        }

        private void ChatForm_Load(object sender, EventArgs e)
        {
            socketCommunication = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            socketCommunication.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

            try
            {
                localIP = new IPEndPoint(IPAddress.Parse(firstForm.yourIpTextBox.Text), Convert.ToInt32(firstForm.yourPortNumericUpDown.Value));
                socketCommunication.Bind(localIP);

                remoteIP = new IPEndPoint(IPAddress.Parse(firstForm.friendsIpTextBox.Text), Convert.ToInt32(firstForm.friendsNumericUpDown.Value));
                socketCommunication.Connect(remoteIP);

                buffer = new Byte[1464];

                socketCommunication.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref remoteIP, new AsyncCallback(callBackOperator), buffer);
            }
            catch (Exception ex)
            {
                if (MessageBox.Show(ex.ToString(), "Error:", MessageBoxButtons.OK, MessageBoxIcon.Error) == DialogResult.OK)
                {
                    firstForm.Close();
                }
            }
        }

        private void callBackOperator(IAsyncResult asyncResult)
        {
            try
            {
                int size = socketCommunication.EndReceiveFrom(asyncResult, ref remoteIP);

                if (size != 0)
                {
                    Byte[] tmp = new Byte[1464];

                    tmp = (Byte[])asyncResult.AsyncState;

                    System.Text.ASCIIEncoding encodingMsg = new System.Text.ASCIIEncoding();
                    string msg = encodingMsg.GetString(tmp);

                    chatListBox.Items.Add("Anonymous: " + msg);
                    chatListBox.SelectedIndex = chatListBox.Items.Count - 1;
                }

                buffer = new Byte[1464];
                socketCommunication.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref remoteIP, new AsyncCallback(callBackOperator), buffer);
            }
            catch (Exception ex)
            {
                if (MessageBox.Show(ex.ToString(), "Error:", MessageBoxButtons.OK, MessageBoxIcon.Error) == DialogResult.OK)
                {
                    firstForm.Close();
                }
            }
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            System.Text.ASCIIEncoding encodingMsg = new System.Text.ASCIIEncoding();
            byte[] msg = new byte[1464];
            msg = encodingMsg.GetBytes(msgTextBox.Text);

            socketCommunication.Send(msg);

            chatListBox.Items.Add("Ja: " + msgTextBox.Text);
            chatListBox.SelectedIndex = chatListBox.Items.Count - 1;
            msgTextBox.Clear();
        }

        private void ChatForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            System.Text.ASCIIEncoding encodingMsg = new System.Text.ASCIIEncoding();
            byte[] msg = new byte[1464];
            msg = encodingMsg.GetBytes("Your friend close chat window, sorry :(");

            socketCommunication.Send(msg);

            firstForm.Close();
        }
    }
}
