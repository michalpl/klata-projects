/**
 * Created by klata on 2014-09-12.
 * 
 * If you have any questions, write: m.platta@wp.pl
 *
 * This is my first serious script, please respectful.
 * 
 * Salut ;)
 */

// CASPER-BOT

var checkAndSigIn = require('./Modules/CheckNamePassAndSigIn.js');
var list = require('./Modules/CountErrorStateList.js');
var multiCheck = require('./Modules/MultiCheck.js');
var dateFunc = require('./Modules/DateFunctions.js');
var getData = require('./Modules/GetDataFromPageAndText.js');
var setAndSelect = require('./Modules/SetAndSelect.js');

var casper = require('casper').create( {
    pageSettings: {
        javascriptEnabled: true,
        localToRemoteUrlAccessEnabled: true,
        loadImages: true,
        loadPlugins: true,
        XSSAuditingEnabled: true
    },
    viewportSize: {
        width: 1024,
        height: 768
    },
    waitTimeout: 30000,
    verbose: false,
    logLevel: 'debug',
    userAgent: 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36',
    clientScripts: [
        './jquery/jquery-2.1.0.min.js'
    ],
    onPageInitialized: list.ifOpenWebSite()
});

casper.allDataRequest = {
    dataToDate: [],
    dataFromDate: [],
    dataLink: [],
    asocArrayWithTomorrwFlight: []
}

/**
 * If data is valid
 *
 * @param {string} toDate - this date must by older than tomorrw date or the same
 * @param {string} fromDate - this date must by younger than tomorrow date or the same
 * @return {boolean}
 */
casper.ifDateValidate = function (toDate, fromDate) {
    var dateGreater = multiCheck.dateAfter(dateFunc.reverseDate(toDate), dateFunc.getTomorrowDate());
    var dateLess = multiCheck.dateBefore(dateFunc.reverseDate(fromDate), dateFunc.getTomorrowDate());

    return dateGreater && dateLess;
};

/**
 * Get valid data to asocjate array
 *
 * @param {string} flightDetails - string with airport and date
 * @param {string} travellers - array with travellers
 */
casper.getValidRequests = function (flightDetails, travellers) {
    var airport = getData.getSubstringBeforeSelector(flightDetails, 'Out:', 0, -15).toString();     // get airpot and date from flightDetails
    var stringDate = getData.getSubstringAfterSelector(flightDetails, 'Out:', 5, 10).toString();    // get only fligth date from flightDetails
    
    var tomorrowDate = new Date(dateFunc.getTomorrowDate());                                        // change tomorrow date to Date format
    var flightDate = new Date(dateFunc.reverseDate(stringDate));                                    // change flight date to Date format

    if (flightDate.getTime() === tomorrowDate.getTime()) {
        if (!(airport in this.allDataRequest.asocArrayWithTomorrwFlight)) {
            this.allDataRequest.asocArrayWithTomorrwFlight[airport] = '';        
        }
        this.allDataRequest.asocArrayWithTomorrwFlight[airport] = this.allDataRequest.asocArrayWithTomorrwFlight[airport].concat(travellers);
    }
};

/**
 * Push all data from request 
 *
 * @param {string[]} fromDate - array with from date
 * @param {string[]} toDate - array with to date
 * @param {string[]} link - array with link to request
 */
casper.pushAllDataRequest = function (fromDate, toDate, link) {
    this.allDataRequest.dataFromDate = this.allDataRequest.dataFromDate.concat(fromDate);
    this.allDataRequest.dataToDate = this.allDataRequest.dataToDate.concat(toDate);
    this.allDataRequest.dataLink = this.allDataRequest.dataLink.concat(link);
};

/**
 * If flight content exists get flight data
 */
casper.waitCheckAndSendValidFlight = function () {
    this.waitForSelector('div.other', function () {
        if (this.exists('div.flight')) {
            var flightDetails = getData.getFirstChild('1');                                         // Get array with flight details
            var travellers = getData.getLastChild('1');                                             // Get array with travelers

            for (var j = 0; j < flightDetails.length; j++) {
                this.getValidRequests(flightDetails[j], travellers);
            }
        }
    });
};

/**
 * If exists more page then redirect to there
 */
casper.ifExistsThenRedirect = function () {
    if (this.exists('img.next')) {
        setAndSelect.setPaginatorAttribute();
        this.clickLabel('Next', 'a');
        this.waitForSelectorTextChange('span.selectorTarget', function () {
            this.getDataFromPage();
        });
    }
}

/**
 * Get data from column request 
 */
casper.getDataFromPage = function () {
    this.pushAllDataRequest(
        getData.getTextFromRequest('div.x-grid3-col-00ND0000005QZsd'),
        getData.getTextFromRequest('div.x-grid3-col-00ND0000005QZsf'),
        this.getElementsAttribute('div.x-grid3-col-Name a', 'href')
    );
    this.ifExistsThenRedirect();
};

// ---------------------------------------------------------------------------------------------
// -------------------------------------- MAIN PROGRAM -----------------------------------------
// ---------------------------------------------------------------------------------------------

// Open website
casper.start('https://login.salesforce.com', function () {
    checkAndSigIn.check();
});

// Fill form
casper.waitForSelector('title', function () {
    var USERNAME = this.cli.get('username').toString();                                             // Get username from cli
    var PASSWORD = this.cli.get('password').toString();                                             // Get password from cli
    
    list.sendState(2);
    checkAndSigIn.fill('form[name = "login"]', USERNAME, PASSWORD);
});

// Check login
casper.waitForSelector('title', function () {
    if (this.exists('title')) {
        if (multiCheck.checkTitle('salesforce.com - Enterprise Edition')) {
            this.clickLabel('Travel Requests', 'a');
        } else if (multiCheck.checkTitle('salesforce.com - Customer Secure Login Page')) {
            list.sendError(1);
        } else if (multiCheck.checkTitle('Unable to Process Request')) {
            list.sendError(5);
        }
    } else {
        list.sendError(3);
    }
});

// Go to all request
casper.waitForSelector("input[name='go']", function () {
    setAndSelect.selectOptions('select#fcf', 0);
    this.mouseEvent('click', 'input[name="go"]');
});

// Click paginator
casper.waitForSelector("input[name='new']", function () {
    this.mouseEvent('click', 'span.selectorTarget');
});

// Change attibute paginator and click option 200 per page
casper.waitForSelector("input[name='new']", function () {
    list.sendState(4);
    if (this.exists('h2#RPPWarningTitle')){
        setAndSelect.selectOptions('select#RPPSelect', 3);
        this.mouseEvent('click', 'input#RPPSaveButton');
        setAndSelect.setPaginatorAttribute();
        this.click('span.left');
        this.clickLabel('200', 'td');
    } else {
        setAndSelect.setPaginatorAttribute();
        this.click('span.left');
        this.clickLabel('200', 'td');
        this.waitForSelectorTextChange('span.selectorTarget', function () {
        });
    }
});

// Get all data and valid
casper.then(function () {
    list.sendState(5);
    this.getDataFromPage();

    var dataValidLink = [];                                             // Array with valid link

    for (var j = 0; j < this.allDataRequest.dataLink.length - 1; j++) {
        if (this.ifDateValidate(this.allDataRequest.dataToDate[j], this.allDataRequest.dataFromDate[j])) {
            dataValidLink.push(this.allDataRequest.dataLink[j]);
        }
    }

    list.sendCount(1, this.allDataRequest.dataLink.length);
    list.sendCount(2, dataValidLink.length);

    var i = 0;                                                          // Counter to show progress

    this.repeat(dataValidLink.length, function () {
        list.sendCount(3, i + 1);
        setAndSelect.changePage(dataValidLink[i]);
        this.waitCheckAndSendValidFlight();
        this.then(function () {
            i++;
        });
    });
});

// Show valid data
casper.then(function () {
    for (i in this.allDataRequest.asocArrayWithTomorrwFlight) {
        this.echo('DATA#' + i + '|' + this.allDataRequest.asocArrayWithTomorrwFlight[i]);
    }
});

// Logout from Kimble
checkAndSigIn.logout();

casper.run(function () {
    this.exit();
});