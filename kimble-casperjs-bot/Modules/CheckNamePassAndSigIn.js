var require = patchRequire(require);
var list = require('./CountErrorStateList.js');

/**
 * Check username, password and load page
 */
exports.check = function () {
	/**
	 * Verifies the existence of a login and password
	 *
	 * @this {checkLogin}
	 */
	casper.checkLogin = function () {
    	return (this.cli.has('username') && this.cli.has('password')); 
   	};

	/**
	 * Verifies the existence of title
	 *
	 * @this {checkLoad}
	 */
	casper.checkLoad = function () {
    	return this.exists('title'); 
	};

	casper.then(function () {
		if (this.checkLogin() && this.checkLoad()) {
			return true;
		} else if (!this.checkLogin()) {
			list.sendError(2);
		} else {
			list.sendError(3);
		}
	});
};

/**
 * Filling login form on website
 *
 * @param {string} loginFormSelector from selector
 * @param {string} username
 * @param {string} password
 * @returns {*}
 */
exports.fill = function (loginFormSelector, username, password) {
	
	casper.fillLoginForm = function (usernamee, passwordd) {
		this.fillSelectors(loginFormSelector, {
        	'[name="username"]': usernamee,
        	'[name="pw"]': passwordd
    	}, true);
	}

	return casper.fillLoginForm(username, password);
};

/**
 * Logout from kimble
 *
 * @returns {logoutSalesforce}
 */
exports.logout = function () {

    /**
     * Click logout button
     */
	casper.logoutClick = function () {
    	this.click('div#userNav');
    	this.clickLabel('Logout', 'a');
	};

	/**
 	* Logout and wait for check
 	*/
	return casper.logoutSalesforce = function () {
    	this.then(function () {
        	this.logoutClick();
    	});

    	this.waitForSelector('form[name="login"]', function () {});
	};
};
