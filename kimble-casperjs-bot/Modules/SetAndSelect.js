var require = patchRequire(require);
/**
 * Select option in selector on website
 *
 * @param {string} selector - selector of selector
 * @param {number} option - index of option
 */
exports.selectOptions = function (selector, option) {
    casper.evaluate(function (selectorr, optionn) {
        document.querySelector(selectorr).selectedIndex = optionn;

        return true;
    }, {
        selectorr: selector,
        optionn: option
    });
};

/**
 * Set formats of displaying data
 *
 * @param {string} name
 * @param {string} reference
 * @param {string} travellers
 * @param {string} status
 * @param {string} activity
 * @param {string} fromDate
 * @param {string} toDate
 * @param {string} dateDetails
 * @param {string} moreDetails
 * @param {string} flightDate
 * @returns {string}
 */
exports.setFormDate = function (name, reference, travellers, status, activity, fromDate, toDate, dateDetails, moreDetails, flightDate) {
    return (
        name + '|' +
        reference + '|' +
        travellers + '|' +
        status + '|' +
        activity + '|' +
        fromDate + '|' +
        toDate + '|' +
        dateDetails + '|' +
        moreDetails + '|' +
        flightDate + '|'
    );
};

/**
 * Set formats of displaying data
 *
 * @param {string} airport
 * @param {string} travellers
 * @returns {string}
 */
exports.setFormDate2 = function (airport, travellers) {
    return (
        airport + '|' +
        travellers + '|'
    );
};

/**
 * Set paginator atribute
 * only for salesforce
 */
exports.setPaginatorAttribute = function () {
    var setAttribute = getData.getPaginatorAttribute();

    casper.evaluate(function (attribute) {
        $('span.selectorTarget').children('table').children('tbody').children('tr').last().attr('onmousedown', attribute);

        return true;
    }, {
        attribute: setAttribute
    });
};

/**
 * Open new request
 *
 * @param {string} link - specjal link to request
 */
exports.changePage = function (link) {
    return casper.open('https://eu1.salesforce.com' + link).then(function () {
    });
};
