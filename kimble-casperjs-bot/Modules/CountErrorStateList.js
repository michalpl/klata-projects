var require = patchRequire(require);

/**
 * Echo first state when page is open
 */
exports.ifOpenWebSite = function () {
    return console.log('STATE#Open-website');
}

/**
 * Send number of check and valid pages
 *
 * @param {number} option - number of status
 * @param {number} number - number of pages
 * @returns {*}
 */
exports.sendCount = function (option, number) {
    switch (option) {
        case 0:
            return casper.echo('INVALID#');                     // Invalid flight
            break;
        case 1:
            return casper.echo('MAX#' + number);                // All requests
            break;
        case 2:
            return casper.echo('VALID#' + number);              // Number of valid request
            break;
        case 3:
            return casper.echo('COUNT#' + number);
            break;
    }
};

/**
 * Send error and exit casperjs
 *
 * @param {number} option - number of error
 * @returns {*}
 */
exports.sendError = function (option) {
    switch (option) {
        case 1:
            return casper.echo('ERROR#1') + casper.exit();      // Bad username or password
            break;
        case 2:
            return casper.echo('ERROR#2') + casper.exit();      // No username or password
            break;
        case 3:
            return casper.echo('ERROR#3') + casper.exit();      // Time out
            break;
        case 4:
            return casper.echo('ERROR#4') + casper.exit();      // Not flights
            break;
        case 5:
            return casper.echo('ERROR#5') + casper.exit();      // Server not found
            break;
        case 6:
            return casper.echo('ERROR#6') + casper.exit();      // Not valid flights
            break;
    }
};

/**
 * Send status of progress
 *
 * @param {number} option - number of step
 * @returns {*}
 */
exports.sendState = function (option) {
    switch (option) {
        case 1:
            return casper.echo('STATE#Open-website');
            break;
        case 2:
            return casper.echo('STATE#Logging');
            break;
        case 3:
            return casper.echo('STATE#Get-my-data');
            break;
        case 4:
            return casper.echo('STATE#Get-all-data');
            break;
        case 5:
            return casper.echo('STATE#Processing-data');
            break;
    }
}