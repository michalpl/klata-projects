var require = patchRequire(require);

/**
 * Change format date with dd/mm/yyyy to mm/dd/yyyy
 * np. get 02/11/1999 return 11/02/1999
 *
 * @param {string} text - date in string
 * @returns {string} changed format to mm/dd/yyyy
 */
exports.reverseDate = function (text) {
    // VARIABLES
    var dd = text,                                          // day from string
        mm = text,                                          // month from string
        yy = text;                                          // year from string

    if (text != ' ') {
        dd = dd.substr(0, 2);
        mm = mm.substr(3, 2);
        yy = yy.substr(6, 4);
    }
    if (text != ' ') {
        return mm + '/' + dd + '/' + yy;
    } else {
        return '';
    }
};

/**
 * Get to day date in string in format mm/d(dd)/yyyy
 *
 * @returns {number} cutToDay
 */
exports.getToDayDate = function () {
    // VARIABLES
    var toDay = new Date();                                 // get to day date in Date format
    var dd = toDay.getDate();                               // get day from toDay in number
    var mm = toDay.getMonth() + 1;                          // get month from toDay in number and plus one
    var yy = toDay.getFullYear();                           // get year from toDay
    var cutToDay = new Date(mm + '/' + dd + '/' + yy);

    return cutToDay.getTime();
};

/**
 * Get tomorrow date in string in format mm/d(dd)/yyyy
 *
 * @returns {string}
 */
exports.getTomorrowDate = function () {
    //VARIABLES
    var toDay = new Date();                                 // get to day date in Date format
    toDay.setDate(toDay.getDate() + 1);                     // add one day to toDay

    var dd = toDay.getDate();                               // get day from toDay in number
    var mm = toDay.getMonth() + 1;                          // get month from toDay in number and plus one
    var yy = toDay.getFullYear();                           // get year from toDay

    return mm + '/' + dd + '/' + yy;
};