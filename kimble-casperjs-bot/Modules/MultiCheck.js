var require = patchRequire(require);
var dateFunc = require('./DateFunctions.js');

/**
 * Check title with current title
 *
 * @param {string} title
 * @returns {boolean}
 */
exports.checkTitle = function (title) {
	return casper.getTitle() === title;
};

/**
 * Check date is older than dates in array or is the same
 *
 * @param {string} date - date in string in format: mm/dd/yyyy
 * @param {string} array - array of date in format: mm/dd/yyyy in string
 * @returns {boolean}
 */
exports.checkDateAfterElementInArray = function (date, array) {
    if (date === '') {
        return true;
    }

    for (var j = 0; j < array.length; j++) {
        var date1 = new Date(date);                                                 // change string date to Date format
        var date2 = new Date(array[j]);                                             // change string date from array to Date format

        if (date1 >= date2) {
            return true;
        }
    }
};

/**
 * Check date is older than date or is the same
 * format is mm/dd/yyyy
 *
 * @param {string} olderDate - date >= than
 * @param {string} youngerDate - date < than
 * @returns {boolean}
 */
exports.dateAfter = function (olderDate, youngerDate) {
    if (olderDate === '') {
        return true;
    }
    
    var date1 = new Date(olderDate);                                                // change string date to Date format
    var date2 = new Date(youngerDate);                                              // change string date to Date format

    if (date1 >= date2) {
        return true;
    }
};

/**
 * Check date is younger than dates in array or is the same
 *
 * @param {string} date - date in string in format: mm/dd/yyyy
 * @param {string[]} array - array of date in format: mm/dd/yyyy in string
 * @returns {boolean}
 */
exports.checkDateBeforeElementInArray = function (date, array) {
    if (date === '') {
        return true;
    }

    for (var j = 0; j < array.length; j++) {
        var date1 = new Date(date);                                                 // change string date to Date format
        var date2 = new Date(array[j]);                                             // change string date from array to Date format

        if (date1 <= date2) {
            return true;
        }
    }
};

/**
 * Check date is younger than date or is the same
 * format is mm/dd/yyyy
 *
 * @param {string} youngerDate
 * @param {string} olderDate
 * @returns {boolean}
 */
exports.dateBefore = function (youngerDate, olderDate) {
    if (youngerDate === '') {
        return true;
    }
    
    var date1 = new Date(youngerDate);
    var date2 = new Date(olderDate);

    if (date1 <= date2) {
        return true;
    }
};

/**
 * looking for element in array
 *
 * @param {*} element
 * @param {string[]} array
 * @returns {boolean}
 */
exports.isElementInArray = function (element, array) {
    var k = 0;
    for (var j = 0; j < array.length; j++) {
        if (element === array[j]) {
            k = 1;
            break;
        }
    }

    return (k === 1);
};

/**
 * Check if the date great than to day date or the same
 *
 * @param {Date} date
 * @returns {boolean}
 */
exports.isActive = function (date) {
    return date.getTime() >= dateFunc.getToDayDate();
};