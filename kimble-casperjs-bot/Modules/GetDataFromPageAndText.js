var require = patchRequire(require);

/**
 * Get array of data from list requests
 * only for salesforce
 *
 * @param {string} selector - selector of request column
 * @returns {string[]}
 */
exports.getTextFromRequest = function (selector) {
    return casper.evaluate(function (selectorsdate) {
        var data = [];
        $(selectorsdate).each(function (index, element) {
            data[index] = $(element).text();
        });

        return data;
    }, {
        selectorsdate: selector
    });
};

/**
 * Get date from column form table first content from request page
 * only for salesforce
 *
 * @param {string} child - number of column (in string)
 * @returns {string[]}
 */
exports.getFirstChild = function (child) {
    return casper.evaluate(function (option) {
        var dataArray = [];

        $('div.travelCategoryContent').first().children('table').children('tbody').children('tr').each(function (index, element) {
            dataArray[index] = $(element).children('td:nth-child(' + option + ')').text().trim();
        });

        return dataArray;
    }, {
        option: child
    });
};

/**
 * Get date from column form table last content from request page
 * only for salesforce
 *
 * @param {string} child - number of column (in string)
 * @returns {string[]}
 */
exports.getLastChild = function (child) {
    return casper.evaluate(function (option) {
        var dataArray = [];
        $('div.travelCategoryContent').last().children('table').children('tbody').children('tr').each(function (index, element) {
            dataArray[index] = $(element).children('td:nth-child(' + option + ')').text();
        });

        return dataArray;
    }, {
        option: child
    });
};

/**
 * Get substring from text after selector
 *
 * @param {string} text - text from getting is substring
 * @param {string} selector - selector after which getting is substring
 * @param {number} start - if 0 substring is get on selector
 * @param {number} length - length of substring
 * @returns {string} text
 */
exports.getSubstringAfterSelector = function (text, selector, start, length) {
    var position = text.indexOf(selector);
    text = text.substr(position + start, length);

    return text;
};

/**
 * Get substring from text before selector
 *
 * @param {string} text - text from getting is substring
 * @param {string} selector - selector before which getting is substring
 * @param {number} start - place where start getting substring
 * @param {number} end - if 0 substring is get to start selector
 * @returns {string} text
 */
exports.getSubstringBeforeSelector = function (text, selector, start, end) {
    text = text.replace('(New Request)', '');
    text = text.trim();
    var position = text.indexOf(selector);
    text = text.substr(start, position - end);

    return text;
};

/**
 * Get attribute from paginator
 * only for salesforce
 *
 * @returns {Object}
 */
exports.getPaginatorAttribute = function () {
    return casper.evaluate(function () {
        return ($('span.selectorTarget').children('table').children('tbody').children('tr').last().attr('onmousedown')).replace('200', '10000');
    });
};