// CASPER-BOT

var checkAndSigIn = require('./Modules/CheckNamePassAndSigIn.js');
var list = require('./Modules/CountErrorStateList.js');
var multiCheck = require('./Modules/MultiCheck.js');
var dateFunc = require('./Modules/DateFunctions.js');
var getData = require('./Modules/GetDataFromPageAndText.js');
var setAndSelect = require('./Modules/SetAndSelect.js');

var casper = require('casper').create( {
    pageSettings: {
        javascriptEnabled: true,
        localToRemoteUrlAccessEnabled: true,
        loadImages: true,
        loadPlugins: true,
        XSSAuditingEnabled: true
    },
    viewportSize: {
        width: 1024,
        height: 768
    },
    waitTimeout: 30000,
    verbose: false,
    logLevel: 'debug',
    userAgent: 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36',
    clientScripts: [
        './jquery/jquery-2.1.0.min.js'
    ],
    onPageInitialized: ifOpenWebSite()
});

casper.myFlightData = {
    flightDate: [],
    references: [],
    airport: []
};

/**
 * Echo first state
 */
function ifOpenWebSite () {
    console.log('STATE#Open-website')
}

/**
 * Open new request
 *
 * @param {string} link
 */
casper.changePage = function (link) {
    this.open('https://eu1.salesforce.com' + link).then(function () {
    });
};

/**
 * Reverse and push data to dataMyFromDate
 *
 * @param text
 */
casper.pushStrToArrayDataMyDate = function (text) {
    this.myFlightData.flightDate.push(dateFunc.reverseDate(text));
};

/**
 * Push data to dataMyReference
 *
 * @param text
 */
casper.pushStrToArrayDataMyRef = function (text) {
    this.myFlightData.references.push(text);
};

/**
 * Push data to dataMyAirport
 *
 * @param text
 */
casper.pushStrToArrayDataMyAirport = function (text) {
    this.myFlightData.airport.push(text);
};

/**
 * Add date to array from array and cut unnecessary, only for salesforce
 *
 * @param {string[]} array
 */
casper.pushAllDateToArrayMyDate = function (array) {
    this.pushStrToArrayDataMyDate(getData.getSubstringAfterSelector(array, 'Out:', 5, 10));
};

/**
 * Add airport to array from array and cut unnecessary, only for salesforce
 *
 * @param {string[]} array
 */
casper.pushAllDateToArrayMyAirport = function (array) {
    this.pushStrToArrayDataMyAirport(getData.getSubstringBeforeSelector(array, 'Out:', 0, -15));
};

/**
 * Push all global variables
 *
 * @param {string[]} array
 * @param {string} element
 * @returns {boolean}
 */
casper.pushAllGlobalVariables = function (array, element) {
    this.pushAllDateToArrayMyAirport(array);
    this.pushAllDateToArrayMyDate(array);
    this.pushStrToArrayDataMyRef(element);
};

casper.isActiveSendMyRequest = function (element, names, references, status, activity, fromDate, toDate, index) {
    var date = new Date(dateFunc.reverseDate(getData.getSubstringAfterSelector(element, 'Out:', 5, 10)));
    var flightDate = getData.getSubstringAfterSelector(element, 'Out:', 5, 10);

    if (multiCheck.isActive(date)) {
        this.pushAllGlobalVariables(element, references);
        this.echo('MY#' + setAndSelect.setFormDate(
            names,
            references,
            getData.getLastChild('1'),
            status,
            activity,
            fromDate,
            toDate,
            getData.getFirstChild('1')[index],
            getData.getFirstChild('2')[index],
            flightDate
        ));
    }
};

/**
 * Send my request and push global variables
 *
 * @param {number} index
 * @param {string} names
 * @param {string} references
 * @param {string} status
 * @param {string} activity
 * @param {string} fromDate
 * @param {string} toDate
 * @param {string[]} array
 */
casper.sendMyRequest = function (index, names, references, status, activity, fromDate, toDate, array) {
    this.then(function () {
        this.isActiveSendMyRequest(array[index], names, references, status, activity, fromDate, toDate, index);
    });
};

/**
 * Wait for selector and if exists selector2 send my request
 *
 * @param {string} names
 * @param {string} references
 * @param {string} status
 * @param {string} activity
 * @param {string} fromDate
 * @param {string} toDate
 */
casper.waitCheckAndSendMyFlight = function (names, references, status, activity, fromDate, toDate) {
    this.waitForSelector('div.other', function () {
        if (this.exists('div.flight')) {
            var child = getData.getFirstChild('1');

            for (var j = 0; j < child.length; j++) {
                this.sendMyRequest(j, names, references, status, activity, fromDate, toDate, child);
            }
        }
    });
};

/**
 * Cut if
 *
 * @param {string} toDate
 * @param {string} references
 * @param {string} fromDate
 * @returns {*|boolean}
 */
casper.ifDateValidate = function (toDate, references, fromDate) {
    var dateGreater = multiCheck.checkDateAfterElementInArray(dateFunc.reverseDate(toDate), this.myFlightData.flightDate);
    var dateLess= multiCheck.checkDateBeforeElementInArray(dateFunc.reverseDate(fromDate), this.myFlightData.flightDate);
    var isElement = !multiCheck.isElementInArray(references, this.myFlightData.references);

    return dateGreater && dateLess && isElement;
};

/**
 * Send valid request
 *
 * @param {number} index
 * @param {string} text
 * @param {string} names
 * @param {string} references
 * @param {string} status
 * @param {string} activity
 * @param {string} fromDate
 * @param {string} toDate
 */
casper.sendValidRequests = function (index, text, names, references, status, activity, fromDate, toDate) {
    var airport = getData.getSubstringBeforeSelector(text, 'Out:', 0, -15);
    var date = getData.getSubstringAfterSelector(text, 'Out:', 5, 10);

    if (multiCheck.isElementInArray(airport, this.myFlightData.airport)) {
        this.echo('DATA#' + setAndSelect.setFormDate(
            names,
            references,
            getData.getLastChild('1'),
            status,
            activity,
            fromDate,
            toDate,
            getData.getFirstChild('1')[index],
            getData.getFirstChild('2')[index],
            date
        ));
    } else {
        list.sendCount(0);
    }
};

/**
 * Wait for selector and if exists selector2 send validate request
 *
 * @param {number} index
 * @param {string} names
 * @param {string} references
 * @param {string} status
 * @param {string} activity
 * @param {string} fromDate
 * @param {string} toDate
 */
casper.waitCheckAndSendValidFlight = function (index, names, references, status, activity, fromDate, toDate) {
    this.waitForSelector('div.other', function () {
        if (this.exists('div.flight')) {
            var child = getData.getFirstChild('1');
            for (var j = 0; j < child.length; j++) {
                this.sendValidRequests(j, child[j], names, references, status, activity, fromDate, toDate);
            }
        } else {
            list.sendCount(0);
        }
    });
};

casper.allDataRequest = {
    dataNames: [],
    dataReferences: [],
    dataStatus: [],
    dataActivity: [],
    dataFromDate: [],
    dataToDate: [],
    dataLink: []
}

casper.pushAllData = function (a,b,c,d,e,f,g) {
    this.allDataRequest.dataNames = this.allDataRequest.dataNames.concat(a);
    this.allDataRequest.dataReferences = this.allDataRequest.dataReferences.concat(b);
    this.allDataRequest.dataStatus = this.allDataRequest.dataStatus.concat(c);
    this.allDataRequest.dataActivity = this.allDataRequest.dataActivity.concat(d);
    this.allDataRequest.dataFromDate = this.allDataRequest.dataFromDate.concat(e);
    this.allDataRequest.dataToDate = this.allDataRequest.dataToDate.concat(f);
    this.allDataRequest.dataLink = this.allDataRequest.dataLink.concat(g);
};

casper.getDataFromPage = function () {
    this.then(function () {
        this.pushAllData(
            getData.getTextFromRequest('div.x-grid3-col-Name span'),
            getData.getTextFromRequest('div.x-grid3-col-00ND0000005QZsY'),
            getData.getTextFromRequest('div.x-grid3-col-00ND0000005QZsc span'),
            getData.getTextFromRequest('div.x-grid3-col-00ND0000005QZsa span'),
            getData.getTextFromRequest('div.x-grid3-col-00ND0000005QZsd'),
            getData.getTextFromRequest('div.x-grid3-col-00ND0000005QZsf'),
            this.getElementsAttribute('div.x-grid3-col-Name a', 'href')
        );
    });

    this.then(function () {
        if (this.exists('img.next')) {
            setAndSelect.setPaginatorAttribute();
            this.clickLabel('Next', 'a');
            this.waitForSelectorTextChange('span.selectorTarget', function () {
                this.getDataFromPage();
            });
        }
    });
};

// ---------------------------------------------------------------------------------------------
// -------------------------------------- MAIN PROGRAM -----------------------------------------
// ---------------------------------------------------------------------------------------------

// Open website
casper.start('https://login.salesforce.com', function () {
    checkAndSigIn.check();
});

// Fill form
casper.waitForSelector('title', function () {
    var USERNAME = this.cli.get('username').toString();
    var PASSWORD = this.cli.get('password').toString();
    
    list.sendState(2);
    checkAndSigIn.fill('form[name = "login"]', USERNAME, PASSWORD);
});

// Check login
casper.waitForSelector('title', function () {
    if (this.exists('title')) {
        if (multiCheck.checkTitle('salesforce.com - Enterprise Edition')) {
            this.clickLabel('Travel Requests', 'a');
        } else if (multiCheck.checkTitle('salesforce.com - Customer Secure Login Page')) {
            list.sendError(1);
        } else if (multiCheck.checkTitle('Unable to Process Request')) {
            list.sendError(5);
        }
    } else {
        list.sendError(3);
    }
});

// My travel request
casper.waitForSelector('title', function () {
    list.sendState(3);
    setAndSelect.selectOptions('select#fcf', 2);
    this.mouseEvent('click', 'input[name="go"]');
});

casper.waitForSelector("input[name='new']", function () {
    if (this.exists('div.x-grid3-col-00ND0000005QZsd')) {
        var dataFromDate = getData.getTextFromRequest('div.x-grid3-col-00ND0000005QZsd');
        var dataNames = getData.getTextFromRequest('div.x-grid3-col-Name span');
        var dataReferences = getData.getTextFromRequest('div.x-grid3-col-00ND0000005QZsY');
        var dataStatus = getData.getTextFromRequest('div.x-grid3-col-00ND0000005QZsc span');
        var dataActivity = getData.getTextFromRequest('div.x-grid3-col-00ND0000005QZsa span');
        var dataToDate = getData.getTextFromRequest('div.x-grid3-col-00ND0000005QZsf');
        var dataLink = this.getElementsAttribute('div.x-grid3-col-Name a', 'href');
        var i = 0;

        this.repeat(dataFromDate.length, function () {
            this.changePage(dataLink[i]);
            this.waitCheckAndSendMyFlight(
                dataNames[i],
                dataReferences[i],
                dataStatus[i],
                dataActivity[i],
                dataFromDate[i],
                dataToDate[i]
            );
            this.then(function () {
                i++;
            });
        });
    } else {
        list.sendError(4);
    }
});

// All open travel request
casper.then(function () {
    if (this.myFlightData.references.length != 0) {
        list.sendState(4);
        this.clickLabel('Travel Requests', 'a');
    } else {
        list.sendError(6);
    }
});

casper.waitForSelector("input[name='go']", function () {
    setAndSelect.selectOptions('select#fcf', 0);
    this.mouseEvent('click', 'input[name="go"]');
});

casper.waitForSelector("input[name='new']", function () {
    this.mouseEvent('click', 'span.selectorTarget');
});

casper.waitForSelector("input[name='new']", function () {
    if (this.exists('h2#RPPWarningTitle')){
        setAndSelect.selectOptions('select#RPPSelect', 3);
        this.mouseEvent('click', 'input#RPPSaveButton');
        setAndSelect.setPaginatorAttribute();
        this.click('span.left');
        this.clickLabel('200', 'td');
    } else {
        setAndSelect.setPaginatorAttribute();
        this.click('span.left');
        this.clickLabel('200', 'td');
        this.waitForSelectorTextChange('span.selectorTarget', function () {
        });
    }
});

// very ugly code ;)
casper.then(function () {
    list.sendState(5);

    this.then(function () {
        this.getDataFromPage();
    });

    this.then(function () {
        var dataTheSameNames = [];
        var dataTheSameReferences = [];
        var dataTheSameStatus = [];
        var dataTheSameActivity = [];
        var dataTheSameFromDate = [];
        var dataTheSameToDate = [];
        var dataTheSameLink = [];

        var i = 0;

        for (var j = 0; j < this.allDataRequest.dataNames.length - 1; j++) {
            if (this.ifDateValidate(this.allDataRequest.dataToDate[j], this.allDataRequest.dataReferences[j], this.allDataRequest.dataFromDate[j])) {
                dataTheSameNames.push(this.allDataRequest.dataNames[j]);
                dataTheSameReferences.push(this.allDataRequest.dataReferences[j]);
                dataTheSameStatus.push(this.allDataRequest.dataStatus[j]);
                dataTheSameActivity.push(this.allDataRequest.dataActivity[j]);
                dataTheSameFromDate.push(this.allDataRequest.dataFromDate[j]);
                dataTheSameToDate.push(this.allDataRequest.dataToDate[j]);
                dataTheSameLink.push(this.allDataRequest.dataLink[j]);
            }
        }

        list.sendCount(1, this.allDataRequest.dataNames.length);
        list.sendCount(2, dataTheSameNames.length);

        this.repeat(dataTheSameNames.length, function () {
            list.sendCount(3, i + 1);
            this.changePage(dataTheSameLink[i]);
            this.waitCheckAndSendValidFlight(i,
                dataTheSameNames[i],
                dataTheSameReferences[i],
                dataTheSameStatus[i],
                dataTheSameActivity[i],
                dataTheSameFromDate[i],
                dataTheSameToDate[i]
            );
            this.then(function () {
                i++;
            });
        });
    });
});

checkAndSigIn.logout();

casper.run(function () {
    this.exit();
});